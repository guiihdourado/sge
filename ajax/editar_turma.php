<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id = filter_input(INPUT_POST, 'id');
$tipo = filter_input(INPUT_POST, 'tipo');

switch ($tipo) {
    case 'busca':
        $turma = new app\models\Turmas();
        $join_turma_curso = "INNER JOIN tb_sge_turma_curso tc on tb_sge_turma.id_turma = tc.id_turma";
        $join_curso = "INNER JOIN tb_sge_curso c on tc.id_curso = c.id_curso";
        $join_turma_periodo = "INNER JOIN tb_sge_turma_periodo tp on tp.id_turma = tb_sge_turma.id_turma";
        $join_periodo = "INNER JOIN tb_sge_periodo p on tp.id_periodo = p.id_periodo";
        $busca_turma = $turma::first(array('select' => 'tc.nr_semestre,tb_sge_turma.nm_turma, tp.id_periodo, tb_sge_turma.tp_turno, tc.id_curso, tb_sge_turma.id_turma', 'joins' => array($join_turma_curso, $join_curso, $join_turma_periodo, $join_periodo), 'conditions' => array('tb_sge_turma.id_turma = ?', $id)));

        $json = $busca_turma->to_json();

        echo json_encode($json);
        break;
    default :
        $nome = filter_input(INPUT_POST, 'nmTurma');
        $periodo = filter_input(INPUT_POST, 'cbPeriodo');
        $turno = filter_input(INPUT_POST, 'tpTurno');
        $curso = filter_input(INPUT_POST, 'cbCurso');
        $semestre = filter_input(INPUT_POST, 'cbSemestre');
        $id_turma = filter_input(INPUT_POST, 'id_turma');

        $turma = new app\models\Turmas();
        $turma_curso = new app\models\TurmasCursos();
        $turma_periodo = new app\models\TurmasPeriodos();

        $attributes_turma = array(
            'nm_turma' => $nome,
            'tp_turno' => $turno
        );
        $r_turma = $turma->atualizar($id_turma, $attributes_turma);

        $turma_curso->delete_all(array('conditions' => array('tb_sge_turma_curso.id_turma = ?', $id_turma)));
        
        $attr_turma_curso = array(
            'id_turma' => (int) $id_turma,
            'id_curso' => (int) $curso,
            'nr_semestre' => (int)$semestre
        );
        $r_turma_curso = $turma_curso->cadastrar($attr_turma_curso);
        
        $turma_periodo->delete_all(array('conditions' => array('tb_sge_turma_periodo.id_turma = ?', $id_turma)));
        
        $attr_turma_perio = array(
            'id_turma' => (int) $id_turma,
            'id_periodo' => (int) $periodo
        );
        
        $r_turma_periodo = $turma_periodo->cadastrar($attr_turma_perio);
        
        $_SESSION['variavel'] = 'sucesso_alterar_turma';
        
        break;
}
