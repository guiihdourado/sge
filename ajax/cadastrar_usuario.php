<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$matricula = filter_input(INPUT_POST, 'matricula');
$senha = filter_input(INPUT_POST, 'senha');

$tipo = substr($matricula, 4, 1);
$usuario = new app\models\Usuarios();

if ($tipo == 1) {
    $aluno = new app\models\Alunos();

    $busca_aluno = $aluno->find('first', array('conditions' => array('nr_matricula = ?', $matricula)));

    if ($busca_aluno == null) {
        echo 'A matrícula não é válida, confira a matrícula ou entre em contato com a administração da instituição !';
    } else {
        $attributes_usuario = array(
            'id_pessoa' => $busca_aluno->id_pessoa,
            'nm_senha' => md5($senha),
            'tp_usuario' => 1
        );
        $usuario->cadastrar($attributes_usuario);

        echo 'Cadastrado com Sucesso !';
    }
} elseif ($tipo == 2) {
    $professor = new app\models\Professores();

    $busca_professor = $professor->find('first', array('conditions' => array('nr_matricula = ?', $matricula)));

    if ($busca_professor == null) {
        echo 'A matrícula não é válida, confira a matrícula ou entre em contato com a administração da instituição !';
    } else {
        $attributes_usuario = array(
            'id_pessoa' => $busca_professor->id_pessoa,
            'nm_senha' => md5($senha),
            'tp_usuario' => 2
        );
        $usuario->cadastrar($attributes_usuario);

        echo 'Cadastrado com Sucesso !';
    }
} else {
    echo 'A matrícula não é válida, confira a matrícula ou entre em contato com a administração da instituição !';
}