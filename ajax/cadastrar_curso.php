<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$formulario = array();
parse_str($_POST['formulario'], $formulario);

$nome = $formulario['nmCurso'];
$descricao = $formulario['descricaoCurso'];
$carga_horaria = $formulario['cargaCurso'];
$tipo = $formulario['tpCurso'];
$semestre = $formulario['qtdSem'];
$objDisciplina = json_decode($_POST['objDisc']);

$curso = new app\models\Curso();
$disciplinaCurso = new app\models\DisciplinaCurso();

$attributes_curso = array(
    'nr_semestre' => (int) $semestre,
    'nm_curso' => $nome,
    'tp_grau' => $tipo,
    'ds_descricao' => $descricao,
    'nr_carga_horaria' => $carga_horaria,
    'tp_area' => 1
);
$r_curso = $curso->cadastrar($attributes_curso);

if ($r_curso) {
    foreach ($objDisciplina as $obj) {
        foreach ($obj as $is) {
            $att_disc_curso = array(
                'id_curso' => (int) $r_curso->id_curso,
                'id_disciplina' => $is->id,
                'nr_semestre' => $is->semestre
            );
            $disciplinaCurso->cadastrar($att_disc_curso);
        }
    }
    if($disciplinaCurso){
        
        echo 'Cadastrado com Sucesso';
        $_SESSION['variavel'] = 'sucesso_cadastrar_curso';
    }
    else{
        echo 'Erro ao cadastrar, tente novamente !';
        $_SESSION['variavel'] = 'erro_cadastrar_curso';
    }
} else {
    echo 'Erro ao cadastrar, tente novamente !';
}