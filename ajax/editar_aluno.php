<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/public/functions/cpf.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/public/functions/rg.php';

$id = filter_input(INPUT_POST, 'id');
$tipo = filter_input(INPUT_POST, 'tipo');

switch ($tipo) {
    case 'busca':
        $pessoa = new app\models\Pessoas();
        $join_aluno = 'INNER JOIN tb_sge_aluno a ON(a.id_pessoa = tb_sge_pessoa.id_pessoa)';
        $join_endereco = 'INNER JOIN tb_sge_endereco e ON(e.id_endereco = tb_sge_pessoa.id_endereco)';
        $join_estados = 'INNER JOIN tb_sge_estados es ON(es.id = e.id_estado)';
        $join_cidades = 'INNER JOIN tb_sge_cidades c ON(c.id = e.id_cidade)';
        $busca_aluno = $pessoa::first(array('select' => '*', 'joins' => array($join_aluno, $join_endereco, $join_estados, $join_cidades), 'conditions' => array('tb_sge_pessoa.id_pessoa = ?', $id)));
        $json = $busca_aluno->to_json();

        echo json_encode($json);
        break;
    default :
        $nome_completo = filter_input(INPUT_POST, 'nome_completo');
        $sexo = filter_input(INPUT_POST, 'sexo');
        $cpf = filter_input(INPUT_POST, 'cpf');
        $rg = filter_input(INPUT_POST, 'rg');
        $dt_nasc = filter_input(INPUT_POST, 'dt_nasc');
        $nome_pai = filter_input(INPUT_POST, 'nome_pai');
        $nome_mae = filter_input(INPUT_POST, 'nome_mae');
        $telefone = filter_input(INPUT_POST, 'telefone');
        $celular = filter_input(INPUT_POST, 'celular');
        $email = filter_input(INPUT_POST, 'email');
        $endereco = filter_input(INPUT_POST, 'endereco');
        $bairro = filter_input(INPUT_POST, 'bairro');
        $estado = filter_input(INPUT_POST, 'estado');
        $cidade = filter_input(INPUT_POST, 'cidade');
        $cep = filter_input(INPUT_POST, 'cep');
        $id_curso = filter_input(INPUT_POST, 'curso');
        $id_aluno = filter_input(INPUT_POST, 'id_aluno');

        $pessoa = new app\models\Pessoas();
        $endereco_model = new app\models\Enderecos();
        $aluno = new app\models\Alunos();

        $attributes_pes = array(
            'nm_pessoa' => $nome_completo,
            'tp_sexo' => (int) $sexo,
            'dt_nascimento' => $dt_nasc,
            'nr_rg' => retira_pontos_rg($rg),
            'ds_email' => $email,
            'nr_cpf' => retira_pontos_cpf($cpf),
            'nm_pai' => $nome_pai,
            'nm_mae' => $nome_mae,
            'nr_celular' => $celular,
            'nr_telefone' => $telefone
        );

        $r_pessoa = $pessoa->atualizar($id_aluno, $attributes_pes);
        
        $attributes_end = array(
            'nm_endereco' => $endereco,
            'nm_bairro' => $bairro,
            'nr_cep' => $cep,
            'id_estado' => (int) $estado,
            'id_cidade' => (int) $cidade
        );

        $busca_pessoa = $pessoa->find($id_aluno);
        $id_endereco = $busca_pessoa->id_endereco;
        
        $endereco_model->atualizar($id_endereco, $attributes_end);


        $attributes_aluno = array(
            'id_curso' => $id_curso
        );

        $aluno_where = $aluno::first(array('conditions' => array('id_pessoa=?', $id_aluno)));
        $aluno_where->update_attributes($attributes_aluno);

        echo 'Atualizado com Sucesso';
        $_SESSION['variavel'] = 'sucesso_alterar_aluno';

        break;
}
