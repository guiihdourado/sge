<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id = filter_input(INPUT_POST, 'id');


$pessoa = new app\models\Pessoas();
$join_aluno = 'INNER JOIN tb_sge_aluno a ON(a.id_pessoa = tb_sge_pessoa.id_pessoa)';
$join_curso = 'INNER JOIN tb_sge_curso cs ON (cs.id_curso = a.id_curso)';
$join_endereco = 'INNER JOIN tb_sge_endereco e ON(e.id_endereco = tb_sge_pessoa.id_endereco)';
$join_estados = 'INNER JOIN tb_sge_estados es ON(es.id = e.id_estado)';
$join_cidades = 'INNER JOIN tb_sge_cidades c ON(c.id = e.id_cidade)';
$busca_aluno = $pessoa::first(array('select' => '*', 'joins' => array($join_aluno, $join_curso, $join_endereco, $join_estados, $join_cidades), 'conditions' => array('tb_sge_pessoa.id_pessoa = ?', $id)));
$json = $busca_aluno->to_json();

echo json_encode($json);
