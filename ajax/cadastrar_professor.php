<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/public/functions/cpf.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/public/functions/rg.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/public/functions/matricula.php';

$nome_completo = filter_input(INPUT_POST, 'nome_completo');
$sexo = filter_input(INPUT_POST, 'sexo');
$cpf = filter_input(INPUT_POST, 'cpf');
$rg = filter_input(INPUT_POST, 'rg');
$dt_nasc = filter_input(INPUT_POST, 'dt_nasc');
$nome_pai = filter_input(INPUT_POST, 'nome_pai');
$nome_mae = filter_input(INPUT_POST, 'nome_mae');
$telefone = filter_input(INPUT_POST, 'telefone');
$celular = filter_input(INPUT_POST, 'celular');
$email = filter_input(INPUT_POST, 'email');
$endereco = filter_input(INPUT_POST, 'endereco');
$bairro = filter_input(INPUT_POST, 'bairro');
$estado = filter_input(INPUT_POST, 'estado');
$cidade = filter_input(INPUT_POST, 'cidade');
$cep = filter_input(INPUT_POST, 'cep');

$pessoa = new app\models\Pessoas();
$endereco_model = new app\models\Enderecos();
$professor = new app\models\Professores();

$attributes_end = array(
    'nm_endereco' => $endereco,
    'nm_bairro' => $bairro,
    'nr_cep' => $cep,
    'id_estado' => (int) $estado,
    'id_cidade' => (int) $cidade
);
$r_endereco = $endereco_model->cadastrar($attributes_end);

$pessoa_rs = $pessoa::first(array('conditions' => array('nr_cpf = ?', retira_pontos_cpf($cpf))));

if ($pessoa_rs == null) {
    if ($r_endereco) {
        $attributes_pes = array(
            'id_endereco' => (int) $r_endereco->id_endereco,
            'nm_pessoa' => $nome_completo,
            'tp_sexo' => (int) $sexo,
            'dt_nascimento' => $dt_nasc,
            'nr_rg' => retira_pontos_rg($rg),
            'ds_email' => $email,
            'nr_cpf' => retira_pontos_cpf($cpf),
            'nm_pai' => $nome_pai,
            'nm_mae' => $nome_mae,
            'nr_celular' => $celular,
            'nr_telefone' => $telefone
        );
        $r_pessoa = $pessoa->cadastrar($attributes_pes);
        if ($r_pessoa) {
            $matricula = gera_matricula_professor();
            $attributes_professor = array(
                'id_pessoa' => (int) $r_pessoa->id_pessoa,
                'nr_matricula' => $matricula
            );
            $r_professor = $professor->cadastrar($attributes_professor);

            echo 'Cadastrado com Sucesso';
            $_SESSION['variavel'] = 'sucesso_cadastrar_professor';
        } else {
            echo 'Erro ao cadastrar, tente novamente !';
            $_SESSION['variavel'] = 'erro_cadastrar_professor';
        }
    }
} else {
    echo 'CPF já cadastrado !';
}