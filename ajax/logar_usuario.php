<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$matricula = filter_input(INPUT_POST, 'matricula');
$senha = filter_input(INPUT_POST, 'senha');
$senha_md5 = md5($senha);

$tipo = substr($matricula, 4, 1);
$pessoa = new app\models\Pessoas();
$usuario = new app\models\Usuarios();

if ($tipo == 1) {
    $join = 'INNER JOIN tb_sge_aluno tsa ON(tsa.id_pessoa = tb_sge_pessoa.id_pessoa) '
            . 'INNER JOIN tb_sge_usuario tsu ON(tsu.id_pessoa = tsa.id_pessoa)';
    $busca_usuario = $pessoa::first(array('select' => 'id_usuario, nm_pessoa, nr_matricula, nm_senha, tp_usuario', 'joins' => $join, 'conditions' => array('tsa.nr_matricula = ? AND tsu.nm_senha = ?', $matricula, $senha_md5)));

    if ($busca_usuario) {
        $_SESSION['usuario_logado'] = true;
        $_SESSION['usuario_nome'] = $busca_usuario->nm_pessoa;
        $_SESSION['usuario_id'] = $busca_usuario->id_usuario;
        $_SESSION['tp_usuario'] = $busca_usuario->tp_usuario;
        session_regenerate_id();
        echo 'Aluno';
    } else {
        echo 'erro';
    }
} elseif ($tipo == 2) {
    $join = 'INNER JOIN tb_sge_professor tsp ON(tsp.id_pessoa = tb_sge_pessoa.id_pessoa) '
            . 'INNER JOIN tb_sge_usuario tsu ON(tsu.id_pessoa = tsp.id_pessoa)';
    $busca_usuario = $pessoa::first(array('select' => 'id_usuario, nm_pessoa, nr_matricula, nm_senha, tp_usuario', 'joins' => $join, 'conditions' => array('tsp.nr_matricula = ? AND tsu.nm_senha = ?', $matricula, $senha_md5)));

    if ($busca_usuario) {
        $_SESSION['usuario_logado'] = true;
        $_SESSION['usuario_nome'] = $busca_usuario->nm_pessoa;
        $_SESSION['usuario_id'] = $busca_usuario->id_usuario;
        $_SESSION['tp_usuario'] = $busca_usuario->tp_usuario;
        session_regenerate_id();
        echo 'Professor';
    } else {
        echo 'erro';
    }
}elseif($matricula == 'Admin' AND $senha == 'admin'){
    $_SESSION['usuario_logado'] = true;
    $_SESSION['usuario_nome'] = 'Administrador';
    $_SESSION['usuario_id'] = 4;
    $_SESSION['tp_usuario'] = 3;
    session_regenerate_id();
    echo 'Admin';
}  else {
    echo 'erro';
}