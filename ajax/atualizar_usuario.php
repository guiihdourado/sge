<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id_usuario = filter_input(INPUT_POST, 'id_usuario');
$nm_exibicao = filter_input(INPUT_POST, 'nome_exibicao');
$senha = filter_input(INPUT_POST, 'new_senha');

$usuario_model = \app\models\Usuarios::find($id_usuario);

if ($senha == '') {
    $attributes_usuario = array(
        'nm_exibicao' => $nm_exibicao
    );

    $usuario_model->update(array('nm_exibicao'=>$nm_exibicao, array('id_usuario' => array($id_usuario))));
    $_SESSION['variavel'] = 'sucesso_alterar_usuario';
} else {
    $attributes_usuario = array(
        'nm_exibicao' => $nm_exibicao,
        'new_senha' => md5($senha)
    );
    $usuario_model->atualizar($id_usuario, $attributes_usuario);
    $_SESSION['variavel'] = 'sucesso_alterar_usuario';
}

