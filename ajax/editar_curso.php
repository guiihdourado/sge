<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id = filter_input(INPUT_POST, 'id');
$tipo = filter_input(INPUT_POST, 'tipo');

switch ($tipo) {
    case 'busca':
        $curso = new app\models\Curso();
        $disciplinaCurso = new app\models\DisciplinaCurso();
        $join_disciplina = 'INNER JOIN tb_sge_disciplina d ON(d.id_disciplina = tb_sge_disciplina_curso.id_disciplina)';
        $busca_curso = $curso::first(array('select' => '*', 'conditions' => array('tb_sge_curso.id_curso = ?', $id)));

        $busca_disciplina = $disciplinaCurso::find('all', array('select' => '*', 'joins' => array($join_disciplina), 'conditions' => array('tb_sge_disciplina_curso.id_curso = ?', $id)));

        $json = new stdClass();
        $json->curso = $busca_curso->to_json();

        foreach ($busca_disciplina as $bc) {
            $retorno[] = array(
                'id' => $bc->id_disciplina,
                'nome' => $bc->nm_disciplina,
                'semestre' => $bc->nr_semestre,
                'carga' => $bc->nr_carga_horaria,
                'nota' => $bc->nr_media_minima
            );
        }
        $json->disciplinas = $retorno;

        echo json_encode($json);
        break;
    default :

        $formulario = array();
        parse_str($_POST['formulario'], $formulario);

        $nome = $formulario['nmCurso'];
        $descricao = $formulario['descricaoCurso'];
        $carga_horaria = $formulario['cargaCurso'];
        $tipo = $formulario['tpCurso'];
        $semestre = $formulario['qtdSem'];
        $id = $formulario['id_curso'];
        $objDisciplina = json_decode($_POST['objDisc']);

        $curso = new app\models\Curso();
        $disciplinaCurso = new app\models\DisciplinaCurso();

        $attributes_curso = array(
            'nr_semestre' => (int) $semestre,
            'nm_curso' => $nome,
            'tp_grau' => $tipo,
            'ds_descricao' => $descricao,
            'nr_carga_horaria' => $carga_horaria,
            'tp_area' => 1
        );
        $r_curso = $curso->atualizar($id_curso, $attributes_curso);

        if ($r_curso) {
            foreach ($objDisciplina as $is) {
                $att_disc_curso = array(
                    'id_curso' => (int) $r_curso->id_curso,
                    'id_disciplina' => $is->id,
                    'nr_semestre' => $is->semestre
                );
                $disciplinaCurso->atualizar($is->id, $att_disc_curso);
            }

            echo 'Atualizado com Sucesso';
             $_SESSION['variavel'] = 'sucesso_alterar_curso';
             break;
       }
}