<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$autor = filter_input(INPUT_POST, 'autor');
$titulo = filter_input(INPUT_POST, 'titulo');
$mensagem = filter_input(INPUT_POST, 'mensagem');

$noticias = new app\models\Noticias();

$attributes_noticia = array(
    'nm_autor' => $autor,
    'nm_titulo' => $titulo,
    'nm_mensagem' => $mensagem
);

$r_noticia = $noticias->cadastrar($attributes_noticia);

if($r_noticia){
    echo "Cadastrado com Sucesso !";
}  else {
    echo "Erro ao cadastrar, tente novamente !";
}


