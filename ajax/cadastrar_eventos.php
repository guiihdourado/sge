<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/public/functions/data.php';

$titulo = filter_input(INPUT_POST, 'titulo');
$dia = filter_input(INPUT_POST, 'dia');
$hora = filter_input(INPUT_POST, 'hora');
$descricao = filter_input(INPUT_POST, 'descricao');
$id_usuario = $_SESSION['usuario_id'];

$evento = new app\models\Eventos();

$attributes_evento = array(
    'data_evento' => date_converter($dia),
    'ds_evento' => $descricao,
    'titulo_evento' => $titulo,
    'id_usuario' => $id_usuario,
    'hora_evento' => $hora
);

$rs_cadastrar = $evento->cadastrar($attributes_evento);

if($rs_cadastrar){
    echo 'Cadastrado com Sucesso !';
}else{
    echo 'Erro ao Cadastrar !';
}


