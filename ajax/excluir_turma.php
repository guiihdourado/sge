<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id = filter_input(INPUT_POST, 'id');
$tipo = filter_input(INPUT_POST, 'tipo');
$turma = new app\models\Turmas();
$turma_periodo = new app\models\TurmasPeriodos();

switch ($tipo) {
    case 'validar':
        $retorno = new stdClass();
        $join_aluno = "INNER JOIN tb_sge_turma_aluno ta on tb_sge_turma.id_turma = ta.id_turma";
        $busca_turma_aluno = $turma::first(array('select' => 'tb_sge_turma.id_turma', 'joins' => array($join_aluno), 'conditions' => array('tb_sge_turma.id_turma = ?', $id)));

        if ($busca_turma_aluno) {
            $retorno->sucesso = false;
            $retorno->vinculo = "Aluno";
            echo json_encode($retorno);
        } else {
            $retorno->sucesso = true;
            $retorno->vinculo = "";
            echo json_encode($retorno);
        }
        break;
    default :
        $turma_periodo->delete_all(array('conditions' => array('tb_sge_turma_periodo.id_turma = ?', $id)));
        $turma->deletar($id);
        $_SESSION['variavel'] = 'sucesso_excluir_turma';
        break;
}
