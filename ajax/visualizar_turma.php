<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id = filter_input(INPUT_POST, 'id');


$turma = new app\models\Turmas();
$join_turma_curso = "INNER JOIN tb_sge_turma_curso tc on tb_sge_turma.id_turma = tc.id_turma";
$join_curso = "INNER JOIN tb_sge_curso c on tc.id_curso = c.id_curso";
$join_turma_periodo = "INNER JOIN tb_sge_turma_periodo tp on tp.id_turma = tb_sge_turma.id_turma";
$join_periodo = "INNER JOIN tb_sge_periodo p on tp.id_periodo = p.id_periodo";
$busca_turma = $turma::first(array('select' => 'tc.nr_semestre,tb_sge_turma.nm_turma, p.nr_ano, p.nr_periodo, tb_sge_turma.tp_turno, tc.id_curso', 'joins' => array($join_turma_curso, $join_curso, $join_turma_periodo, $join_periodo), 'conditions' => array('tb_sge_turma.id_turma = ?', $id)));

$json = $busca_turma->to_json();

echo json_encode($json);
