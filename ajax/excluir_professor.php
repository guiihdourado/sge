<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id = filter_input(INPUT_POST, 'id');

$pessoa = new app\models\Pessoas();
$professor = new app\models\Professores();
$endereco = new app\models\Enderecos();
$professor_disciplina = new app\models\ProfessorDisciplina();
$join_professor = 'INNER JOIN tb_sge_professor p ON(p.id_pessoa = tb_sge_pessoa.id_pessoa)';
$join_endereco = 'INNER JOIN tb_sge_endereco e ON(e.id_endereco = tb_sge_pessoa.id_endereco)';
$busca_professor = $pessoa::first(array('select' => 'tb_sge_pessoa.id_pessoa, tb_sge_pessoa.id_endereco, p.id_professor', 'joins' => array($join_professor, $join_endereco), 'conditions' => array('tb_sge_pessoa.id_pessoa = ?', $id)));

$busca_pd = $professor_disciplina::all(array('conditions' => array('tb_sge_professor_disciplina.id_professor = ?', $busca_professor->id_professor)));

if ($busca_pd != null) {
    
    $professor_disciplina::delete_all(array('conditions' => array('id_professor = ?', $busca_professor->id_professor)));
}

$professor->deletar($busca_professor->id_professor);
$pessoa->deletar($busca_professor->id_pessoa);
$endereco->deletar($busca_professor->id_endereco);

echo 'Deletado com Sucesso';
$_SESSION['variavel'] = 'sucesso_excluir_professor';