<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id = filter_input(INPUT_POST, 'id');

$pessoa = new app\models\Pessoas();
$aluno = new app\models\Alunos();
$endereco = new app\models\Enderecos();
$join_aluno = 'INNER JOIN tb_sge_aluno a ON(a.id_pessoa = tb_sge_pessoa.id_pessoa)';
$join_endereco = 'INNER JOIN tb_sge_endereco e ON(e.id_endereco = tb_sge_pessoa.id_endereco)';
$busca_aluno = $pessoa::first(array('select' => 'tb_sge_pessoa.id_pessoa, tb_sge_pessoa.id_endereco, a.id_aluno', 'joins' => array($join_aluno, $join_endereco), 'conditions' => array('tb_sge_pessoa.id_pessoa = ?', $id)));


$aluno->deletar($busca_aluno->id_aluno);
$pessoa->deletar($busca_aluno->id_pessoa);
$endereco->deletar($busca_aluno->id_endereco);

$_SESSION['variavel'] = 'sucesso_excluir_aluno';



