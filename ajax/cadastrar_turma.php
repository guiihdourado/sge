<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$nome = filter_input(INPUT_POST, 'nmTurma');
$periodo = filter_input(INPUT_POST, 'cbPeriodo');
$turno = filter_input(INPUT_POST, 'tpTurno');
$curso = filter_input(INPUT_POST, 'cbCurso');
$semestre = filter_input(INPUT_POST, 'cbSemestre');


$turma = new app\models\Turmas();
$turma_curso = new app\models\TurmasCursos();
$turma_periodo = new app\models\TurmasPeriodos();

$attributes_turma = array(
    'nm_turma' => $nome,
    'tp_turno' => $turno
);
$r_turma = $turma->cadastrar($attributes_turma);

if ($r_turma) {
    $attr_turma_curso = array(
        'id_turma' => (int) $r_turma->id_turma,
        'id_curso' => (int) $curso,
        'nr_semestre' => $semestre
    );

    $r_turma_curso = $turma_curso->cadastrar($attr_turma_curso);
    if ($r_turma_curso) {
        $attr_turma_perio = array(
            'id_turma' => (int) $r_turma->id_turma,
            'id_periodo' => (int) $periodo
        );
        
        $r_turma_periodo = $turma_periodo->cadastrar($attr_turma_perio);
        
        if($r_turma_periodo){
            $_SESSION['variavel'] = 'sucesso_cadastrar_turma';
           
        } else{
            $_SESSION['variavel'] = 'erro_cadastrar_turma';
        }
    } 
} 






