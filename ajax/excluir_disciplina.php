<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id = filter_input(INPUT_POST, 'id');
$tipo = filter_input(INPUT_POST, 'tipo');

$disciplina = new app\models\Disciplina();
$professor_disciplina = new app\models\ProfessorDisciplina();

switch ($tipo) {
    case 'validar':
        $retorno = new stdClass();
        $join_curso = "INNER JOIN tb_sge_disciplina_curso dc on tb_sge_disciplina.id_disciplina = dc.id_disciplina";
        $busca_disciplina = $disciplina::first(array('select' => 'tb_sge_disciplina.id_disciplina', 'joins' => array($join_curso), 'conditions' => array('tb_sge_disciplina.id_disciplina = ?', $id)));
        if ($busca_disciplina) {
            $retorno->sucesso = false;
            $retorno->vinculo = "Curso";
            echo json_encode($retorno);
        } else {
            $join_turma = "INNER JOIN tb_sge_disciplina_turma dt on tb_sge_disciplina.id_disciplina = dt.id_disciplina";
            $busca_disciplina = $disciplina::first(array('select' => 'tb_sge_disciplina.id_disciplina', 'joins' => array($join_turma), 'conditions' => array('tb_sge_disciplina.id_disciplina = ?', $id)));
            if ($busca_disciplina) {
                $retorno->sucesso = false;
                $retorno->vinculo = "Turma";
                echo json_encode($retorno);
            } else {
                $retorno->sucesso = true;
                $retorno->vinculo = "";
                echo json_encode($retorno);
            }
        }
        break;
    default :
        $disciplina->deletar($id);
        $_SESSION['variavel'] = 'sucesso_excluir_disciplina';
        break;
}

