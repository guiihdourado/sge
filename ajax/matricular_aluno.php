<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id_turma = filter_input(INPUT_POST, 'turmas');
$id_aluno = filter_input(INPUT_POST, 'id_aluno');

$turma_aluno = new app\models\TurmaAluno();

$attributes_ta = array(
    'id_aluno' => $id_aluno,
    'id_turma' => $id_turma
);

$turma_aluno->cadastrar($attributes_ta);

$_SESSION['variavel'] = 'sucesso_matricular_aluno';

