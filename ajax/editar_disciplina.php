<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id = filter_input(INPUT_POST, 'id');
$tipo = filter_input(INPUT_POST, 'tipo');

switch ($tipo) {
    case 'busca':
        $disciplina = new app\models\Disciplina();
        $join_professor = "INNER JOIN tb_sge_professor_disciplina pd on tb_sge_disciplina.id_disciplina = pd.id_disciplina";
        $busca_disciplina = $disciplina::first(array('select' => '*', 'joins' => array($join_professor), 'conditions' => array('tb_sge_disciplina.id_disciplina = ?', $id)));

        $json = $busca_disciplina->to_json();

        echo json_encode($json);
        break;
    default :
        $nome = filter_input(INPUT_POST, 'disc');
        $carga_horaria = filter_input(INPUT_POST, 'grade');
        $percentual_falta = filter_input(INPUT_POST, 'falta');
        $media = filter_input(INPUT_POST, 'media');
        $professor = filter_input(INPUT_POST, 'select_prof');
        $select_disc = filter_input(INPUT_POST, 'select_disc');
        $id_disciplina = filter_input(INPUT_POST, 'id_disciplina');

        $disciplinas = new app\models\Disciplina();
        $professor_disciplina = new app\models\ProfessorDisciplina();
        
        $professor_disciplina->delete_all(array('conditions' => array('tb_sge_professor_disciplina.id_disciplina = ?', $id_disciplina)));

        $attributes_disc = array(
            'id_disciplina_depende' => ($select_disc == "") ? NULL : (int) $select_disc,
            'nm_disciplina' => $nome,
            'nr_carga_horaria' => (int) $carga_horaria,
            'nr_percentual_faltas' => (int) $percentual_falta,
            'nr_media_minima' => (float) $media
        );
        $r_disciplina = $disciplinas->atualizar($id_disciplina, $attributes_disc);
        
        $attr_prof_disc = array(
            'id_professor' => (int) $professor,
            'id_disciplina' => (int) $id_disciplina,
        );

        $r_prof_disc = $professor_disciplina->cadastrar($attr_prof_disc);
        
        
        $_SESSION['variavel'] = 'sucesso_alterar_disciplina';

        break;
}
