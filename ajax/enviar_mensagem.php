<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id_usuario = $_SESSION['usuario_id'];
$id_destinatario = filter_input(INPUT_POST, 'destinatario');
$assunto = filter_input(INPUT_POST, 'assunto');
$mensagem = filter_input(INPUT_POST, 'mensagem');
$visto = 0;

$attributes_mensagem = array(
    'id_usuario' => $id_usuario,
    'id_usuario_destino' => $id_destinatario,
    'ds_mensagem' => $mensagem,
    'msm_vista' => $visto,
    'msm_assunto' => $assunto
);

$mensagem_model = new app\models\Mensagens();

if($mensagem_model->cadastrar($attributes_mensagem)){
    $_SESSION['variavel'] = 'mensagem_enviada_sucesso';
}else{
    $_SESSION['variavel'] = 'mensagem_enviada_erro';
}


