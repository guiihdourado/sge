<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$nome = filter_input(INPUT_POST, 'disc');
$carga_horaria = filter_input(INPUT_POST, 'grade');
$percentual_falta = filter_input(INPUT_POST, 'falta');
$media = filter_input(INPUT_POST, 'media');
$professor = filter_input(INPUT_POST, 'select_prof');
$select_disc = filter_input(INPUT_POST, 'select_disc');


$disciplinas = new app\models\Disciplina();
$professor_disciplina = new app\models\ProfessorDisciplina();

$attributes_disc = array(
    'id_disciplina_depende' => ($select_disc == "") ? NULL : (int)$select_disc,
    'nm_disciplina' => $nome,
    'nr_carga_horaria' => (int) $carga_horaria,
    'nr_percentual_faltas' => (int) $percentual_falta,
    'nr_media_minima' => (float) $media
);
$r_disciplina = $disciplinas->cadastrar($attributes_disc);

if ($r_disciplina) {
    $attr_prof_disc = array(
        'id_professor' => (int) $professor,
        'id_disciplina' => (int) $r_disciplina->id_disciplina,
    );

    $r_prof_disc = $professor_disciplina->cadastrar($attr_prof_disc);
    if ($r_prof_disc) {
        $_SESSION['variavel'] = 'sucesso_cadastrar_disciplina';
    } else {
         $_SESSION['variavel'] = 'erro_cadastrar_disciplina';
    }
} else {
    $_SESSION['variavel'] = 'erro_cadastrar_disciplina';
}






