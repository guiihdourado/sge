<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$curso = new app\models\Curso();

$id = filter_input(INPUT_POST, 'idCurso');

$busca_curso = $curso->find('all', array('conditions' => array('id_curso = ?', $id)));
if ($busca_curso == null) {
    echo "erro";
} else {
    foreach ($busca_curso as $bc) {
        $retorno[] = array(
            'id' => $bc->id_curso,
            'nr' => $bc->nr_semestre
        );
    }
    echo json_encode($retorno);
}





