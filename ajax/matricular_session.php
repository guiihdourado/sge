<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';
$id_aluno = filter_input(INPUT_POST, 'id');

$pessoas = new app\models\Pessoas();
$join_aluno = 'INNER JOIN tb_sge_aluno a ON(a.id_pessoa = tb_sge_pessoa.id_pessoa)';
$busca_alunos = $pessoas::first(array('select' => '*', 'joins' => array($join_aluno), 'conditions' => array('a.id_aluno = ?', $id_aluno)));

$turma = new app\models\Turmas();
$join_turma_curso = "INNER JOIN tb_sge_turma_curso tc on tb_sge_turma.id_turma = tc.id_turma";
$join_curso = "INNER JOIN tb_sge_curso c on tc.id_curso = c.id_curso";
$join_turma_periodo = "INNER JOIN tb_sge_turma_periodo tp on tp.id_turma = tb_sge_turma.id_turma";
$join_periodo = "INNER JOIN tb_sge_periodo p on tp.id_periodo = p.id_periodo";
$join_especializacao = "INNER JOIN tb_sge_tipo_especializacao te on te.id_especializacao = tb_sge_turma.tp_turno";
$busca_turma = $turma::find('all', array('select' => '*', 'joins' => array($join_turma_curso, $join_curso, $join_turma_periodo, $join_periodo, $join_especializacao), 'conditions' => array('tc.id_curso = ?', $busca_alunos->id_curso)));

foreach ($busca_turma as $bc) {
    $retorno[] = array(
        'id_turma' => $bc->id_turma,
        'nm_turma' => $bc->nm_turma,
        'nm_especializacao' => $bc->nm_especializacao,
        'nr_ano' => $bc->nr_ano,
        'nr_periodo' => $bc->nr_periodo
    );
}

echo json_encode($retorno);


