<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id_mensagem = filter_input(INPUT_POST, 'id');

$mensagem = new app\models\Mensagens();
$join_usuario = 'INNER JOIN tb_sge_usuario u ON u.id_usuario = tb_sge_mensagem.id_usuario';
$join_pessoa = 'INNER JOIN tb_sge_pessoa p ON p.id_pessoa = u.id_pessoa';
$rs_mensagem = $mensagem::first(array('select' => '*', 'joins' => array($join_usuario, $join_pessoa), 'conditions' => array('tb_sge_mensagem.id_mensagem = ?', $id_mensagem)));
$json = $rs_mensagem->to_json();

echo json_encode($json);