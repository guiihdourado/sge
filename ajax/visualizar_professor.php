<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

$id = filter_input(INPUT_POST, 'id');


$pessoa = new app\models\Pessoas();
$join_professor = 'INNER JOIN tb_sge_professor p ON(p.id_pessoa = tb_sge_pessoa.id_pessoa)';
$join_endereco = 'INNER JOIN tb_sge_endereco e ON(e.id_endereco = tb_sge_pessoa.id_endereco)';
$join_estados = 'INNER JOIN tb_sge_estados es ON(es.id = e.id_estado)';
$join_cidades = 'INNER JOIN tb_sge_cidades c ON(c.id = e.id_cidade)';
$busca_professor = $pessoa::first(array('select' => '*', 'joins' => array($join_professor,$join_endereco, $join_estados, $join_cidades), 'conditions' => array('tb_sge_pessoa.id_pessoa = ?', $id)));
$json = $busca_professor->to_json();

echo json_encode($json);
