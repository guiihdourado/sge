$(document).ready(function () {
    var div_hide_select = $("#div-hide-select");

    $(function () {
        div_hide_select.find("td[id='editavel']").click(function () {
            var conteudoOriginal = $(this).text();

            $(this).addClass("celulaEmEdicao");
            $(this).html("<input type='text' value='" + conteudoOriginal + "' />");
            $(this).children().first().focus();

            $(this).children().first().keypress(function (e) {
                if (e.which == 13) {
                    var novoConteudo = $(this).val();
                    $(this).parent().text(novoConteudo);
                    $(this).parent().removeClass("celulaEmEdicao");
                }
            });

            $(this).children().first().blur(function () {
                $(this).parent().text(conteudoOriginal);
                $(this).parent().removeClass("celulaEmEdicao");
            });


        });
    });
});