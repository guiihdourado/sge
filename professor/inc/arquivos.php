<div class="span12">
    <div class="div-admin-breadcrum">
        <?php
        if ($_SERVER['QUERY_STRING'] == 'p=arquivos') {
            ?>
            <ul class="breadcrumb">
                <li><a href="?p=home">Início</a> <span class="divider">/</span></li>
                <li class="active">arquivos</li>
            </ul>
        </div>

        <div class="div-btn-novo-admin">

            <button class="btn btn-success" id="bt-admin-novo">Novo <i class="icon-plus"></i></button>

        </div>

        <?php
    }
    ?>





    <div class="widget widget-table action-table">

        <div class="widget-header"> <i class="icon-th-list"></i>

            <h3>Arquivos</h3>



        </div>
        <div class="widget-content">

            <div class="padding-div">

                <div id="div-admin-table">

                    <div class="table-responsive padding-table" >

                        <table class="table table-striped table-bordered table-hover" id="dataTables-admin">
                            
                         

                            <thead>

                                <tr>

                                    <th>Arquivo</th>
                                    <th>Data</th>
                                    <th>Turma</th>
                                    <th style="width: 150px; max-width: 150px; min-width: 150px;">Ações</th>
                                </tr>

                            </thead>

                            <tbody>
                                <tr>
                                    
                                    <td>1ª Aula: Slide 1</td>
                                    <td>01/03</td>
                                    <td>SI 3AN</td>
                                    <td class="actions-admin"><a href="javascript:;" class="btn btn-primary" title="Editar"><i class="btn-icon-only icon-edit"> </i></a><a href="javascript:;" class="btn btn-warning" title="Visualizar"><i class="btn-icon-only icon-eye-open"></i></a><a href="javascript:;" class="btn btn-danger" title="Excluir"><i class="btn-icon-only icon-remove"> </i></a></td>

                               </tr>
                               
                                <tr>
                                    
                                    <td>2ª Aula: Slide 2</td>
                                    <td>08/03</td>
                                    <td>TADS 3AN</td>
                                    <td class="actions-admin"><a href="javascript:;" class="btn btn-primary" title="Editar"><i class="btn-icon-only icon-edit"> </i></a><a href="javascript:;" class="btn btn-warning" title="Visualizar"><i class="btn-icon-only icon-eye-open"></i></a><a href="javascript:;" class="btn btn-danger" title="Excluir"><i class="btn-icon-only icon-remove"> </i></a></td>

                               </tr>
                            </tbody>

                        </table>

                    </div>

                </div>

            </div>

            <div id="div-admin-novo">

                <form class="form-horizontal" method="post" action='inc/upload_files.php' enctype="multipart/form-data">

                    <div class="control-group">											

                        <label class="control-label" for="turma">Turma</label>


                        <div class="controls">
                            <select class="span3" name="turma">
                                <option value=''>Selecione</option>
                                <option value='1'>TADS 1AN</option>
                                <option>TADS 2AN</option>
                                <option>TADS 3AN</option>
                                <option>TADS 4AN</option>
                                <option>TADS 5AN</option>

                            </select>

                        </div> <!-- /controls -->				

                    </div> <!-- /control-group -->


                    <div class="control-group">											

                        <label class="control-label" for="assunto">Assunto</label>

                        <div class="controls">

                            <input type="text" class="span3" id="assunto"  placeholder="Assunto da Mensagem">

                        </div> <!-- /controls -->				

                    </div> <!-- /control-group -->

                    <div class="control-group">

                        <label class="control-label" for="envio">Arquivo: </label>



                        <div class="controls">


                            <div style="position:relative;">
                                <a class='btn btn-primary' href='javascript:;'>
                                    Selecionar arquivo:
                                    <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="arquivo" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                                </a>
                                &nbsp;
                                <span class='label label-info' id="upload-file-info"></span>
                            </div>

                        </div>


                    </div> 
                    <div class="control-group">

                        <div class="controls">

                            <button type="submit" class="btn btn-primary">Enviar</button> 

                            <button type="reset" class="btn">Limpar</button>

                            <button class="btn btn-danger" id="bt-admin-fechar">Cancelar</button>

                        </div>

                    </div>

                </form>

            </div>

        </div>
        

    </div>

    <!-- /widget-content --> 

</div>