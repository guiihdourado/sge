<div class="span12">
    <div class="div-admin-breadcrum">
        <?php
        if ($_SERVER['QUERY_STRING'] == 'p=noticias') {
            ?>
            <ul class="breadcrumb">
                <li><a href="?p=home">Início</a> <span class="divider">/</span></li>
                <li class="active">Notícias</li>
            </ul>
        </div>
    
        <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

                <h3>Notícias</h3>

            </div>

            <!-- /widget-header -->

            <div class="widget-content">
                <div id="div-admin-table">

                    <div class="table-responsive padding-table" >

                        <table class="table table-striped table-bordered table-hover" id="dataTables-admin">

                            <thead>

                                <tr>

                                    <th>Autor</th>
                                    <th>Data</th>
                                    <th>Título</th>

                                </tr>

                            </thead>

                            <tbody>
                                <tr>

                                <td style="width: 200px;"><a href="?p=noticias&id=1">Administrador</a></td>

                                <td style="width: 100px;">06/10</td>

                                <td><a href="?p=noticias&id=1">Notas do seminário já estão disponíveis no blog</a></td>

                            </tr>
                            
                            <tr>

                                <td style="width: 200px;"><a href="?p=noticias&id=2">Administrador</a></td>

                                <td style="width: 100px;">10/10</td>

                                <td><a href="?p=noticias&id=2">Como estão os estágios na área de TI</a></td>

                            </tr>
                            
                            <tr>

                                <td style="width: 200px;"><a href="?p=noticias&id=3">Administrador</a></td>

                                <td style="width: 100px;">20/10</td>

                                <td><a href="?p=noticias&id=3">Inscrições para Palestra - Open Source</a></td>

                            </tr>
                            
                            <tr>

                                <td style="width: 200px;"><a href="?p=noticias&id=4">Administrador</a></td>

                                <td style="width: 100px;">22/10</td>

                                <td><a href="?p=noticias&id=4">Notas da prova A1 já estão disponíveis no blog</a></td>
                            </tr>
                            </tbody>

                        </table>

                    </div>

                </div>

            </div>
            <?php
        } else {
            ?>
            <ul class="breadcrumb">
                <li><a href="?p=home">Início</a> <span class="divider">/</span></li>
                <li><a href="?p=noticias">Notícias</a> <span class="divider">/</span></li>
                <li class="active"> 
                    <?php
                    $id = strrchr($_SERVER['REQUEST_URI'], "id");
                    switch ($id) {
                        case 'id=1':
                            echo "Administrador - Notas do seminário já estão disponíveis no blog";
                            break;
                        case 'id=2':
                            echo "Administrador - Como estão os estágios na área de TI";
                            break;
                        case 'id=3':
                            echo "Administrador - Inscrições para Palestra - Open Source";
                            break;
                        case 'id=4':
                            echo "Administrador - Notas da prova A1 já estão disponíveis no blog";
                            break;
                    }
                    ?>
                </li>
            </ul>

            <div class="widget widget-table action-table">

                <div class="widget-header"> <i class="icon-th-list"></i>

                    <h3>Notícias</h3>

                </div>

                <!-- /widget-header -->

                <div class="widget-content">

                    <div class="table-responsive padding-table" >

                        <?php
                        $id = strrchr($_SERVER['REQUEST_URI'], "id");
                        switch ($id) {
                            case 'id=1':
                                include 'noticias/1.php';
                                break;
                            case 'id=2':
                                include 'noticias/2.php';
                                break;
                            case 'id=3':
                                include 'noticias/3.php';
                                break;
                            case 'id=4':
                                include 'noticias/4.php';
                                break;
                        }
                        ?>

                    </div>

                </div>

            </div>

            <?php
        }
        ?>

    </div>
</div>

