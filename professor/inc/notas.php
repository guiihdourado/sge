<div class="span12">
    <div class="div-admin-breadcrum">
        <?php
        if ($_SERVER['QUERY_STRING'] == 'p=notas') {
            ?>
            <ul class="breadcrumb">
                <li><a href="?p=home">Início</a> <span class="divider">/</span></li>
                <li class="active">Notas</li>
            </ul>
        </div>

        <?php
    }
    ?>

    <div class="widget widget-table action-table">

        <div class="widget-header"> <i class="icon-th-list"></i>

            <h3>Notas</h3>

        </div>

        <!-- /widget-header -->

        <div class="widget-content">

            <div class="padding-div">

                <div id="div-select">
                    <div class="control-group">

                        <label class="control-label" for="prof">Selecione a Turma </label>

                        <div class="controls">

                            <select id="select-prof" name="select-prof">

                                <option value="1"></option>
                                <option value="1">TADS 1AN</option>

                                <option value="1">TADS 2AN</option>

                                <option value="1">TADS 3AN</option>

                                <option value="1">TADS 4AN</option>

                            </select>

                        </div>

                    </div>
                </div>
                <div id="div-hide-select">
                    <ul class="nav nav-tabs" role="tablist" id="myTab">

                        <li class="active"><a href="#projeto-integrado-2">Projeto Integrado II</a></li>
                        <li class=""><a href="#projeto-integrado-3">Projeto Integrado III</a></li>
                        <li class=""><a href="#eng-software-1">Engenharia de Software I</a></li>
                        <li class=""><a href="#eng-software-2">Engenharia de Software II</a></li>

                    </ul>
                    <!-- /widget-header -->

                    <div class="tab-content">
                        <div id="projeto-integrado-2" class="tab-pane fade in active">
                            <div class="table-responsive padding-table" >

                                <table class="table table-striped table-bordered table-hover tabelaEditavel" id="dataTables-admin">

                                    <thead>

                                        <tr>

                                            <th>Nome</th>

                                            <th style="width: 50px">A1</th>

                                            <th style="width:50px">A2</th>

                                            <th style="width: 50px;">Média</th>


                                        </tr>

                                    </thead>

                                    <tbody>                 
                                        <tr>
                                            <td>Maria Cardoso</td>
                                            <td id="editavel">6.5</td>
                                            <td id="editavel">5.5</td>
                                            <td>6.0</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>Rostand Junio</td>
                                            <td id="editavel">6.5</td>
                                            <td id="editavel">5.5</td>
                                            <td>6.0</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>Douglas Cardoso</td>
                                            <td id="editavel">6.5</td>
                                            <td id="editavel">5.5</td>
                                            <td>6.0</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>Jacke Ribeiro</td>
                                            <td id="editavel">6.5</td>
                                            <td id="editavel">5.5</td>
                                            <td>6.0</td>
                                        </tr>

                                    </tbody>

                                </table>

                            </div>
                        </div>

                        <div id="projeto-integrado-3" class="tab-pane fade">
                            <div class="table-responsive padding-table" >

                                <table class="table table-striped table-bordered table-hover" id="dataTables-admin2">

                                    <thead>

                                        <tr>

                                            <th>Nome</th>

                                            <th style="width: 50px">A1</th>

                                            <th style="width:50px">A2</th>

                                            <th style="width: 50px;">Ação</th>

                                            <th style="width: 50px;">Média</th>

                                            <th style="width: 100px;">Status</th>


                                        </tr>

                                    </thead>

                                    <tbody>                 
                                        <tr>
                                            <td>Marcos Sousa</td>
                                            <td><input type="text" class="span1" name="a1" value="9.0" disabled></td>
                                            <td><input type="text" class="span1" name="a2" value="9.0" disabled></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td><input type="text" class="span1" name="media" value="9.0" disabled></td>
                                            <td><input type="text" class="span2" name="status" value="Aprovado" disabled></td>
                                        </tr>

                                        <tr>
                                            <td>Késsia Santos</td>
                                            <td><input type="text" class="span1" name="a1" value="3.0" disabled></td>
                                            <td><input type="text" class="span1" name="a2" value="1.0" disabled></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td><input type="text" class="span1" name="media" value="2.0" disabled></td>
                                            <td><input type="text" class="span2" name="status" value="Reprovado" disabled></td>
                                        </tr>

                                        <tr>
                                            <td>Matheus Carlos</td>
                                            <td><input type="text" class="span1" name="a1" value="3.0" disabled></td>
                                            <td><input type="text" class="span1" name="a2"></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td>--</td>
                                            <td>--</td>
                                        </tr>

                                        <tr>
                                            <td>Carolline Cruz</td>
                                            <td><input type="text" class="span1" name="a1" value="4.0" disabled></td>
                                            <td><input type="text" class="span1" name="a2"></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td>--</td>
                                            <td>--</td>
                                        </tr>

                                        <tr>
                                            <td>Gustavo Henrique</td>
                                            <td><input type="text" class="span1" name="a1" value="0" disabled></td>
                                            <td><input type="text" class="span1" name="a2" value="5.0" disabled=""></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td>--</td>
                                            <td>--</td>
                                        </tr>

                                    </tbody>

                                </table>

                            </div>
                        </div>

                        <div id="eng-software-1" class="tab-pane fade">
                            <div class="table-responsive padding-table" >

                                <table class="table table-striped table-bordered table-hover" id="dataTables-admin3">

                                    <thead>

                                        <tr>

                                            <th>Nome</th>

                                            <th style="width: 50px">A1</th>

                                            <th style="width:50px">A2</th>

                                            <th style="width: 50px;">Ação</th>

                                            <th style="width: 50px;">Média</th>

                                            <th style="width: 100px;">Status</th>


                                        </tr>

                                    </thead>

                                    <tbody>                 
                                        <tr>
                                            <td>Katarine Souza</td>
                                            <td><input type="text" class="span1" name="a1" value="6.5" disabled></td>
                                            <td><input type="text" class="span1" name="a2" value="5.5" disabled></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td><input type="text" class="span1" name="media" value="6.0" disabled></td>
                                            <td><input type="text" class="span2" name="status" value="Aprovado" disabled></td>
                                        </tr>

                                        <tr>
                                            <td>Luziane Silva</td>
                                            <td><input type="text" class="span1" value="3.0" disabled></td>
                                            <td><input type="text" class="span1" value="5.0" disabled></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td><input type="text" class="span1" name="media" value="4.0" disabled></td>
                                            <td><input type="text" class="span2" name="status" value="Reprovado" disabled></td>
                                        </tr>

                                        <tr>
                                            <td>Douglas Silva</td>
                                            <td><input type="text" class="span1" name="a1" value="--" disabled></td>
                                            <td><input type="text" class="span1" name="a2" value="--" disabled=""></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td><input type="text" class="span1" name="media" value="0.0" disabled></td>
                                            <td><input type="text" class="span2" name="status" value="Reprovado" disabled></td>
                                        </tr>

                                        <tr>
                                            <td>Leonardo Aquino</td>
                                            <td><input type="text" class="span1" name="a1" value="8.0" disabled></td>
                                            <td><input type="text" class="span1" name="a2" value="8.0" disabled></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td><input type="text" class="span1" name="media" value="9.0" disabled></td>
                                            <td><input type="text" class="span2" name="status" value="Aprovado" disabled></td>
                                        </tr>

                                        <tr>
                                            <td>Fernanda Castro</td>
                                            <td><input type="text" class="span1" name="a1" value="9.0" disabled></td>
                                            <td><input type="text" class="span1" name="a2"></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td>--</td>
                                            <td>--</td>
                                        </tr>

                                    </tbody>

                                </table>

                            </div>
                        </div>

                        <div id="eng-software-2" class="tab-pane fade">
                            <div class="table-responsive padding-table" >

                                <table class="table table-striped table-bordered table-hover" id="dataTables-admin4">

                                    <thead>

                                        <tr>

                                            <th>Nome</th>

                                            <th style="width: 50px">A1</th>

                                            <th style="width:50px">A2</th>

                                            <th style="width: 50px;">Ação</th>

                                            <th style="width: 50px;">Média</th>

                                            <th style="width: 100px;">Status</th>


                                        </tr>

                                    </thead>

                                    <tbody>                 
                                        <tr>
                                            <td>Bianca Cristina</td>
                                            <td><input type="text" class="span1" name="a1" value="7.5" disabled></td>
                                            <td><input type="text" class="span1" name="a2" value="7.5" disabled></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td><input type="text" class="span1" name="media" value="7.5" disabled></td>
                                            <td><input type="text" class="span2" name="status" value="Aprovado" disabled></td>
                                        </tr>

                                        <tr>
                                            <td>Guilherme Henrique</td>
                                            <td><input type="text" class="span1" name="a1" value="2.0" disabled></td>
                                            <td><input type="text" class="span1" name="a2" value="4.0" disabled></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td><input type="text" class="span1" name="media" value="3.0" disabled></td>
                                            <td><input type="text" class="span2" name="status" value="Reprovado" disabled></td>
                                        </tr>

                                        <tr>
                                            <td>Cristina</td>
                                            <td><input type="text" class="span1" name="a1" value="5.0" disabled></td>
                                            <td><input type="text" class="span1" name="a2"></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td>--</td>
                                            <td>--</td>
                                        </tr>

                                        <tr>
                                            <td>Jorel Vasconcelos</td>
                                            <td><input type="text" class="span1" name="a1" value="8.0" disabled></td>
                                            <td><input type="text" class="span1" name="a2"></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td>--</td>
                                            <td>--</td>
                                        </tr>

                                        <tr>
                                            <td>Eduardo Silva</td>
                                            <td><input type="text" class="span1" name="a1" value="5.0" disabled></td>
                                            <td><input type="text" class="span1" name="a2"></td>
                                            <td><button class="btn btn-success">OK</button></td>
                                            <td>--</td>
                                            <td>--</td>
                                        </tr>

                                    </tbody>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

</div>