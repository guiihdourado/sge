<div class="span12">
    <div class="div-admin-breadcrum">
        <?php
        if ($_SERVER['QUERY_STRING'] == 'p=chamadas') {
            ?>
            <ul class="breadcrumb">
                <li><a href="?p=home">Início</a> <span class="divider">/</span></li>
                <li class="active">Chamada</li>
            </ul>
        </div>

        <?php
    }
    ?>

    <div class="widget widget-table action-table">

        <div class="widget-header"> <i class="icon-th-list"></i>

            <h3>Chamada</h3>

        </div>

        <!-- /widg2eader -->

        <div class="widget-content">

            <div class="padding-div">

                <div id="div-select">
                    <div class="control-group">

                        <label class="control-label" for="prof">Selecione a Turma </label>

                        <div class="controls">

                            <select id="select-prof" name="select-prof">
                                <option value=""></option>
                                <?php
                                $usuario = new app\models\Usuarios();

                                $rs_usuario = $usuario::first(array('conditions' => array('tb_sge_usuario.id_usuario = ?', $_SESSION['usuario_id'])));

                                $professor = new app\models\Professores();
                                $rs_professor = $professor::first(array('conditions' => array('tb_sge_professor.id_pessoa = ?', $rs_usuario->id_pessoa)));

                                $turma_curso = new app\models\TurmasCursos();
                                $join_turma = 'INNER JOIN tb_sge_turma t on t.id_turma = tb_sge_turma_curso.id_turma';
                                $join_disciplina_curso = 'INNER JOIN tb_sge_disciplina_curso dc ON dc.nr_semestre = tb_sge_turma_curso.nr_semestre';
                                $join_disciplina = 'INNER JOIN tb_sge_disciplina disc ON disc.id_disciplina = dc.id_disciplina';
                                $join_professor_disciplina = 'INNER JOIN tb_sge_professor_disciplina pd ON pd.id_disciplina = disc.id_disciplina';
                                $join_professor = 'INNER JOIN tb_sge_professor prof ON prof.id_professor = pd.id_professor';
                                $join_pessoa = 'INNER JOIN tb_sge_pessoa pp ON pp.id_pessoa = prof.id_pessoa';
                                $busca_turmas = $turma_curso::all(array('select' => '*', 'joins' => array($join_turma, $join_disciplina_curso, $join_disciplina, $join_professor_disciplina, $join_professor), 'conditions' => array('prof.id_professor = ?', $rs_professor->id_professor), 'group' => 't.nm_turma'));

                                foreach ($busca_turmas as $bt):
                                    ?>
                                    <option data-id="<?php echo $bt->id_turma; ?>" value="<?php echo $bt->id_turma; ?>"><?php echo $bt->nm_turma; ?></option>
                                <?php endforeach; ?>
                            </select>

                        </div>

                    </div>
                </div>

                <div id="div-hide-select">
                    <ul class="nav nav-tabs" role="tablist" id="myTab">

                        <li class="active"><a href="#projeto-integrado-2">Projeto Integrado II</a></li>
                        <li class=""><a href="#projeto-integrado-3">Projeto Integrado III</a></li>
                        <li class=""><a href="#eng-software-1">Engenharia de Software I</a></li>
                        <li class=""><a href="#eng-software-2">Engenharia de Software II</a></li>
                        <li class=""><a href="#projeto-integrado-1">Progeto Integrado I</a><li>

                    </ul>

                    <div class="tab-content">

                        <div id="projeto-integrado-2" class="tab-pane fade in active">
                            <div class="table-responsive padding-table" >

                                <form class="form-horizontal" action="../../ajax/cadastrar_chamada.php" method="POST" id="form-aluno">



                                    <div class="control-group chamada-dia" >                                           

                                        <label class="control-label" for="dt_nasc">Dia da Aula:</label>

                                        <div class="controls">

                                            <input type="text" class="span2" id="dt_aula" name="dt_aula"  placeholder="Dia da Aula">

                                        </div> <!-- /controls -->               

                                    </div> <!-- /control-group -->

                                    <div id="chamada">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Nome do Aluno</th>
                                                    <th>Faltas</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Guilherme Henrique Dourado Carreiro</td>
                                                    <td>
                                                        <div class="control-group">
                                                            <label class="control-label" for="radios"></label>
                                                            <div class="controls">
                                                                <label class="radio inline" for="radios-0">
                                                                    <input type="radio" name="radio1" id="radios-0" value="1" checked="checked">
                                                                    Faltou
                                                                </label>
                                                                <label class="radio inline" for="radios-1">
                                                                    <input type="radio" name="radio1" id="radios-1" value="2">
                                                                    Não Faltou
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Guilherme Henrique Dourado Carreiro</td>
                                                    <td>
                                                        <div class="control-group">
                                                            <label class="control-label" for="radios"></label>
                                                            <div class="controls">
                                                                <label class="radio inline" for="radios-0">
                                                                    <input type="radio" name="radio2" id="radios-0" value="1" checked="checked">
                                                                    Faltou
                                                                </label>
                                                                <label class="radio inline" for="radios-1">
                                                                    <input type="radio" name="radio2" id="radios-1" value="2">
                                                                    Não Faltou
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Guilherme Henrique Dourado Carreiro</td>
                                                    <td>
                                                        <div class="control-group">
                                                            <label class="control-label" for="radios"></label>
                                                            <div class="controls">
                                                                <label class="radio inline" for="radios-0">
                                                                    <input type="radio" name="radio3" id="radios-0" value="1" checked="checked">
                                                                    Faltou
                                                                </label>
                                                                <label class="radio inline" for="radios-1">
                                                                    <input type="radio" name="radio3" id="radios-1" value="2">
                                                                    Não Faltou
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="control-group salvar-chamada">

                                        <div class="controls">

                                            <input type="submit" class="btn btn-primary" id="btn-cadastrar-chamada" value="Salvar"/>
                                            <a class="btn btn-danger" href="?p=aluno">Cancelar</a>

                                        </div>

                                    </div>


                                </form>
                            </div>
                        </div>

                        <div id="projeto-integrado-3" class="tab-pane fade">
                            <div class="table-responsive padding-table" >

                                <table class="table table-striped table-bordered table-hover" id="dataTables-admin2">

                                    <thead>

                                        <tr>

                                            <th>Nome</th>

                                            <th style="width: 150px; max-width: 150px; min-width: 150px;">Faltas</th>

                                        </tr>

                                    </thead>

                                    <tbody class="tbody-faltas">

                                        <tr>
                                            <td>Andersson</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Juan</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Rafael</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Yago</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Fernando</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Fabricio</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>



                                            </td>

                                        </tr>

                                    </tbody>

                                </table>

                            </div>


                        </div>

                        <div id="eng-software-1" class="tab-pane fade">
                            <div class="table-responsive padding-table" >

                                <table class="table table-striped table-bordered table-hover" id="dataTables-admin3">

                                    <thead>

                                        <tr>

                                            <th>Nome</th>

                                            <th style="width: 150px; max-width: 150px; min-width: 150px;">Faltas</th>

                                        </tr>

                                    </thead>

                                    <tbody class="tbody-faltas">

                                        <tr>
                                            <td>Henrique</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Luana</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Carlos</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Pablo</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Bianca</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nunes</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>



                                            </td>

                                        </tr>

                                    </tbody>

                                </table>

                            </div>


                        </div>

                        <div id="eng-software-2" class="tab-pane fade">
                            <div class="table-responsive padding-table" >

                                <table class="table table-striped table-bordered table-hover" id="dataTables-admin4">

                                    <thead>

                                        <tr>

                                            <th>Nome</th>

                                            <th style="width: 150px; max-width: 150px; min-width: 150px;">Faltas</th>

                                        </tr>

                                    </thead>

                                    <tbody class="tbody-faltas">

                                        <tr>
                                            <td>Junio</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Guilherme</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Russo</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Eduardo</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Juliano</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Poter</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>



                                            </td>

                                        </tr>

                                    </tbody>

                                </table>

                            </div>


                        </div>

                        <div id="projeto-integrado-1" class="tab-pane fade" >
                            <div class="table-responsive padding-table" >

                                <table class="table table-striped table-bordered table-hover" id="dataTables-admin5">

                                    <thead>

                                        <tr>

                                            <th>Nome</th>

                                            <th style="width: 150px; max-width: 150px; min-width: 150px;">Faltas</th>

                                        </tr>

                                    </thead>

                                    <tbody class="tbody-faltas">

                                        <tr>
                                            <td>Daniel</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Emanuel</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Israel</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Michael</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Leonardo</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Natanael</td>
                                            <td>
                                                <div>
                                                    <p>1º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>
                                                <div>
                                                    <p>2º Aula -</p><label class="checkbox-inline">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                                                    </label>
                                                </div>



                                            </td>

                                        </tr>

                                    </tbody>

                                </table>

                            </div>


                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>
</div>

</div>

</div>