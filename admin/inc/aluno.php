
<div class="span12">



    <div class="div-btn-novo-admin">

        <button class="btn btn-success" id="bt-admin-novo">Novo <i class="icon-plus"></i></button>

    </div>



    <div class="widget widget-table action-table">
        <?php
        if (isset($_SESSION['variavel'])):
            switch ($_SESSION['variavel']) {
                case 'sucesso_cadastrar_aluno':
                    echo "<div class='alert alert-success'>
                            <a type='button' class='close' data-dismiss='alert'>&times;</a>
                            Cadastrado com Sucesso !
                          </div>";

                    $_SESSION['variavel'] = '';
                    break;
                case 'erro_cadastrar_aluno':
                    echo "<div class='alert alert-danger'>
                            <a class='close' data-dismiss='alert'>&times;</a>
                            Erro ao cadastrar, tente novamente !
                          </div>";
                    $_SESSION['variavel'] = '';
                    break;
                case 'sucesso_alterar_aluno':
                    echo "<div class='alert alert-success'>
                            <a class='close' data-dismiss='alert'>&times;</a>
                            Atualizado com Sucesso !
                          </div>";
                    $_SESSION['variavel'] = '';
                    break;
                case 'sucesso_excluir_aluno':
                    echo "<div class='alert alert-success'>
                            <a class='close' data-dismiss='alert'>&times;</a>
                            Excluído com Sucesso !
                          </div>";
                    $_SESSION['variavel'] = '';
                    break;
                case 'sucesso_matricular_aluno':
                    echo "<div class='alert alert-success'>
                            <a class='close' data-dismiss='alert'>&times;</a>
                            Matrícula realizada com Sucesso !
                          </div>";
                    $_SESSION['variavel'] = '';
                    break;
            }
        endif;
        ?>

        <div class="widget-header"> <i class="icon-th-list"></i>

            <h3>Alunos Cadastrados</h3>

        </div>

        <!-- /widget-header -->

        <div class="widget-content">

            <div id="div-admin-table">

                <div class="table-responsive padding-table" >

                    <table class="table table-striped table-bordered table-hover" id="dataTables-admin">

                        <thead>

                            <tr>

                                <th>Nome</th>
                                <th>Email</th>
                                <th>Matrícula</th>
                                <th style="width: 220px; max-width: 220px; min-width: 220px;">Ações</th>

                            </tr>

                        </thead>

                        <tbody>
                            <?php
                            $pessoas = new app\models\Pessoas();
                            $join_aluno = 'INNER JOIN tb_sge_aluno a ON(a.id_pessoa = tb_sge_pessoa.id_pessoa)';
                            $busca_alunos = $pessoas::all(array('select' => '*', 'joins' => $join_aluno));

                            foreach ($busca_alunos as $ba):
                                ?>
                                <tr>
                                    <td><?php echo $ba->nm_pessoa; ?></td>
                                    <td><?php echo $ba->ds_email; ?></td>
                                    <td><?php echo $ba->nr_matricula ?></td>
                                    <td class="actions-admin">
                                        <a data-id-aluno="<?php echo $ba->id_pessoa; ?>" class="btn btn-primary" id="btn-editar-aluno" title="Alterar"><i class="btn-icon-only icon-edit"> </i></a>
                                        <a data-id-aluno="<?php echo $ba->id_pessoa; ?>" class="btn btn-warning" id="btn-visualizar-aluno" title="Visualizar"><i class="btn-icon-only icon-eye-open"></i></a>
                                        <a class="btn btn-danger excluir-aluno" data-id-aluno="<?php echo $ba->id_pessoa; ?>" title="Excluir"><i class="btn-icon-only icon-remove"> </i></a>
                                        <a class="btn btn-info" href="#myModal" data-toggle="modal" data-target="#myModal" data-id-aluno="<?php echo $ba->id_aluno; ?>" id="btn-matricular-aluno" title="Matricular"><i class="btn-icon-only icon-book"> </i></a>                                    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>

                    </table>

                </div>

            </div>



            <div id="div-admin-novo">

                <form class="form-horizontal" method="POST" id="form-aluno">

                    <div class="control-group">                                           

                        <label class="control-label" for="nome_completo">Nome Completo:</label>

                        <div class="controls">

                            <input type="text" class="span5" name="nome_completo" id="nome_completo" maxlength="100" placeholder="Nome do Aluno">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->



                    <div class="control-group">                                           

                        <label class="control-label">Sexo:</label>

                        <div class="controls">

                            <label class="radio inline">

                                <input type="radio" name="sexo" value="1" id="masculino"> Masculino

                            </label>

                            <label class="radio inline">

                                <input type="radio" name="sexo" id="feminino" value="2"> Feminino

                            </label>

                        </div>    <!-- /controls -->           

                    </div> <!-- /control-group -->



                    <div class="control-group">                                           

                        <label class="control-label" for="cpf">CPF:</label>

                        <div class="controls">

                            <input type="text" class="span2" id="cpf" name="cpf"  placeholder="CPF do Aluno">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="rg">RG:</label>

                        <div class="controls">

                            <input type="text" class="span2" id="rg" name="rg"  placeholder="RG do Aluno">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="dt_nasc">Data de Nascimento:</label>

                        <div class="controls">

                            <input type="text" class="span2" maxlength="10" id="dt_nasc" name="dt_nasc"  placeholder="Data de Nascimento">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="nome_pai">Nome do Pai:</label>

                        <div class="controls">

                            <input type="text" class="span5" id="nome_pai" name="nome_pai"  placeholder="Nome do Pai">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="nome_mae">Nome da Mãe:</label>

                        <div class="controls">

                            <input type="text" class="span5" id="nome_mae" name="nome_mae"  placeholder="Nome da Mãe">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->


                    <div class="control-group">                                           

                        <label class="control-label" for="telefone">Telefone Residencial:</label>

                        <div class="controls">

                            <input type="text" class="span2" id="telefone" name="telefone"  placeholder="Telefone do Aluno">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="celular">Celular:</label>

                        <div class="controls">

                            <input type="text" class="span2" id="celular" name="celular"  placeholder="Celular do Aluno">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="email">E-mail:</label>

                        <div class="controls">

                            <input type="text" class="span4" id="email" maxlength="70" name="email" placeholder="Email do Aluno">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->



                    <div class="control-group">                                           

                        <label class="control-label" for="endereco">Endereço:</label>

                        <div class="controls">

                            <input type="text" class="span4" id="endereco" name="endereco" maxlength="100"  placeholder="Endereço do Aluno">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="bairro">Bairro:</label>

                        <div class="controls">

                            <input type="text" class="span2" id="bairro" name="bairro" maxlength="50"  placeholder="Bairro do Aluno">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->



                    <div class="control-group">

                        <label class="control-label" for="estado">Estado:</label>

                        <div class="controls">

                            <select id="estado" name="estado">


                                <?php
                                $estados = new app\models\Estados();
                                $estados_encontrados = $estados->listar();

                                foreach ($estados_encontrados as $e):
                                    ?>
                                    <option value="<?php echo $e->id; ?>"><?php echo utf8_encode($e->nome); ?></option>
                                    <?php
                                endforeach;
                                ?>


                            </select>
                            <img id="img-loader" src="<?php echo site_url(); ?>public/img/ajax-loader.gif">
                        </div>


                    </div>



                    <div class="control-group">

                        <label class="control-label" for="cidade">Cidade:</label>

                        <div class="controls" id="cidade-div">

                            <select id="cidade" name="cidade" disabled>

                            </select>

                        </div>

                    </div>



                    <div class="control-group">                                           

                        <label class="control-label" for="cep">CEP:</label>

                        <div class="controls">

                            <input type="text" class="span2" id="cep" name="cep" placeholder="CEP do Aluno">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">

                        <label class="control-label" for="curso">Curso:</label>

                        <div class="controls">

                            <select id="curso" name="curso">


                                <?php
                                $cursos = new app\models\Curso();
                                $cursos_encontrados = $cursos->listar();

                                foreach ($cursos_encontrados as $c):
                                    ?>
                                    <option value="<?php echo $c->id_curso; ?>"><?php echo $c->nm_curso; ?></option>
                                    <?php
                                endforeach;
                                ?>


                            </select>
                        </div>


                    </div>



                    <div class="control-group">

                        <div class="controls">

                            <input type="submit" class="btn btn-primary" id="btn-cadastrar-aluno" value="Cadastrar"/>

                            <button type="reset" class="btn">Limpar</button>

                            <a class="btn btn-danger" href="?p=aluno">Cancelar</a>

                        </div>

                    </div>

                </form>

            </div>

            <div id="div-admin-show">

                <form class="form-horizontal" method="POST">

                    <div class="control-group">                                           

                        <label class="control-label" for="nome_completo">Nome Completo:</label>

                        <div class="controls">
                            <div id="nome_completo" class="margin-top3">
                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->



                    <div class="control-group">                                           

                        <label class="control-label">Sexo:</label>

                        <div class="controls">

                            <div id="sexo" class="margin-top3">

                            </div>

                        </div>    <!-- /controls -->           

                    </div> <!-- /control-group -->



                    <div class="control-group">                                           

                        <label class="control-label" for="cpf">CPF:</label>

                        <div class="controls">
                            <div id="cpf" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="rg">RG:</label>

                        <div class="controls">
                            <div id="rg" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="dt_nasc">Data de Nascimento:</label>

                        <div class="controls">
                            <div id="dt_nasc" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="nome_pai">Nome do Pai:</label>

                        <div class="controls">
                            <div id="nm_pai" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="nome_mae">Nome da Mãe:</label>

                        <div class="controls">
                            <div id="nm_mae" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->


                    <div class="control-group">                                           

                        <label class="control-label" for="telefone">Telefone Residencial:</label>

                        <div class="controls">
                            <div id="telefone" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="celular">Celular:</label>

                        <div class="controls">
                            <div id="celular" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="email">E-mail:</label>

                        <div class="controls">
                            <div id="email" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->



                    <div class="control-group">                                           

                        <label class="control-label" for="endereco">Endereço:</label>

                        <div class="controls">
                            <div id="endereco" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="bairro">Bairro:</label>

                        <div class="controls">
                            <div id="bairro" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->



                    <div class="control-group">

                        <label class="control-label" for="estado">Estado:</label>

                        <div class="controls">
                            <div id="estado" class="margin-top3">

                            </div>
                        </div>


                    </div>



                    <div class="control-group">

                        <label class="control-label" for="cidade">Cidade:</label>

                        <div class="controls">

                            <div id="cidade" class="margin-top3">

                            </div>

                        </div>

                    </div>



                    <div class="control-group">                                           

                        <label class="control-label" for="cep">CEP:</label>

                        <div class="controls">
                            <div id="cep" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">

                        <label class="control-label" for="curso">Curso:</label>

                        <div class="controls">
                            <div id="curso" class="margin-top3">

                            </div>
                        </div>

                    </div>

                    <div class="control-group">

                        <label class="control-label" for="curso">Matricula:</label>

                        <div class="controls">
                            <div id="matricula" class="margin-top3">

                            </div>
                        </div>

                    </div>



                    <div class="control-group">

                        <div class="controls">

                            <a class="btn btn-warning" href="?p=aluno">Voltar</a>

                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>

    <!-- /widget-content -->

</div>

<!-- MODAL EXCLUIR -->

<div id="modalExcluir" class="modal hide fade in">
    <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">×</a>
        <h3 id="modalTitulo">Excluir Aluno</h3>
    </div>
    <div class="modal-body">
        <p id="msg"></p>
    </div>
    <div class="modal-footer">
        <button id="btnExcluirAluno" data-id-turma="" class="btn btn-danger">Excluir</button>
    </div>
</div>

<div id="myModal" class="modal fade hide">
    <div class="modal-header">
        <a type="button" class="close" aria-hidden="true">&times;</a>
        <h3>Matricular Aluno</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="POST" id="form-turmas">

            <div class="control-group">                                         

                <label class="control-label" for="titulo">Turmas</label>

                <div class="controls">
                    <div id="divAlert" class="alert hide">
                        <strong>Atenção</strong> Não Existe Turmas Cadastradas!
                    </div>
                    <select class="hide" id="turmas" name="turmas">

                    </select>
                </div>              

            </div> <!-- /control-group -->
    </div>
    <div class="modal-footer">
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-primary" id="btn-matricular-aluno-turma">Matricular</button> 
                <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>
            </div>
        </div>

        </form>

    </div>
</div>
