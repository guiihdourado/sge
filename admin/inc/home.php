<div class="span8">

    <div class="widget">

        <div class="widget-header"> <i class="icon-bookmark"></i>

            <h3>Atalhos Importantes</h3>

        </div>

        <!-- /widget-header -->

        <div class="widget-content">

            <div class="shortcuts"> 
                <a href="?p=mensagens" class="shortcut"><i class="shortcut-icon icon-list-alt"></i><span class="shortcut-label">Mensagem</span> </a>
                <a href="?p=noticias" class="shortcut"><i class="shortcut-icon icon-calendar"></i> <span class="shortcut-label">Noticias</span> </a> </div>


            <!-- /shortcuts --> 

        </div>

        <!-- /widget-content --> 

    </div>

</div>

<div class="span4">

    <!-- /widget -->

    <div class="widget">

        <div class="widget-header"> <i class="icon-bell-alt"></i>

            <h3> Calendário</h3>

            <a id="novo-evento-calendario-a" href="#myModal" data-toggle="modal" data-target="#myModal"><i class="icon-plus-sign" id="novo-evento-calendario" title="Adicionar Evento ao Calendário"></i></a>

        </div>

        <!-- /widget-header -->

        <div class="widget-content">

            <div id="my-calendar"></div>

        </div>

        <!-- /widget-content --> 

    </div>

</div>

<div id="myModal" class="modal fade hide">
    <div class="modal-header">
        <button type="button" class="close" aria-hidden="true">&times;</button>
        <h3>Criar Evento</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" action="POST" id="form-eventos">

            <div class="control-group">                                         

                <label class="control-label" for="titulo">Título</label>

                <div class="controls">

                    <input type="text" class="span3" id="titulo"  name="titulo" placeholder="Título do Evento">

                </div> <!-- /controls -->               

            </div> <!-- /control-group -->



            <div class="control-group">                                         

                <label class="control-label"  for="dia">Dia do Evento</label>

                <div class="controls" id="datepicker">

                    <input type="text" class="span3" id="dia" name="dia" placeholder="Dia do Evento">

                </div> <!-- /controls -->               

            </div> <!-- /control-group -->

            <div class="control-group">                                         

                <label class="control-label" for="hora">Hora</label>

                <div class="controls clockpicker" data-autoclose="true">

                    <input type="text" class="span3" id="hora" name="hora" placeholder="Horas do Evento">

                </div> <!-- /controls -->               

            </div> <!-- /control-group -->



            <div class="control-group">                                         

                <label class="control-label" for="hora">Descrição</label>

                <div class="controls">

                    <textarea id="descricao" class="span3" name="descricao" placeholder="Descrição do Evento"></textarea>

                </div> <!-- /controls -->               

            </div> <!-- /control-group -->




    </div>
    <div class="modal-footer">
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-primary" id="btn-cadastrar-evento">Cadastrar</button> 
                <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>

            </div>

        </div>

        </form>

    </div>
</div>
