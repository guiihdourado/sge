<div class="span12">

    <div class="div-btn-novo-admin">

        <button class="btn btn-success" id="bt-admin-novo">Novo <i class="icon-plus"></i></button>

    </div>

    <div class="widget widget-table action-table">

        <?php
        if (isset($_SESSION['variavel'])):
            switch ($_SESSION['variavel']) {
                case 'sucesso_cadastrar_turma':
                    echo "<div class='alert alert-success'>
                            <a class='close' data-dismiss='alert'>&times;</a>
                            Cadastrado com Sucesso !
                          </div>";
                    $_SESSION['variavel'] = '';
                    break;
                case 'erro_cadastrar_turma':
                    echo "<div class='alert alert-danger'>
                            <a class='close' data-dismiss='alert'>&times;</a>
                            Erro ao Cadastrar!Tente Novamente
                          </div>";
                    $_SESSION['variavel'] = '';
                    break;
                case 'sucesso_alterar_turma':
                    echo "<div class='alert alert-success'>
                            <a class='close' data-dismiss='alert'>&times;</a>
                            Alterado com Sucesso !
                          </div>";
                    $_SESSION['variavel'] = '';
                    break;
                case 'sucesso_excluir_turma':
                    echo "<div class='alert alert-success'>
                            <a class='close' data-dismiss='alert'>&times;</a>
                            Excluido com Sucesso !
                          </div>";
                    $_SESSION['variavel'] = '';
                    break;
            }
        endif;
        ?>

        <div class="widget-header"> <i class="icon-th-list"></i>

            <h3>Turmas Cadastradas</h3>

        </div>

        <!-- /widget-header -->

        <div class="widget-content">

            <div id="div-admin-table">

                <div class="table-responsive padding-table" >

                    <table class="table table-striped table-bordered table-hover" id="dataTables-admin">

                        <thead>

                            <tr>

                                <th>Nome</th>
                                <th>Curso</th>
                                <th>Período</th>
                                <th style="width: 150px; max-width: 150px; min-width: 150px;">Ações</th>

                            </tr>

                        </thead>

                        <tbody>
                            <?php
                            $turma = new app\models\Turmas();
                            $join_turma_curso = "INNER JOIN tb_sge_turma_curso tc on tb_sge_turma.id_turma = tc.id_turma";
                            $join_curso = "INNER JOIN tb_sge_curso c on tc.id_curso = c.id_curso";
                            $join_turma_periodo = "INNER JOIN tb_sge_turma_periodo tp on tp.id_turma = tb_sge_turma.id_turma";
                            $join_periodo = "INNER JOIN tb_sge_periodo p on tp.id_periodo = p.id_periodo";
                            $busca_turma = $turma::find('all', array('select' => '*', 'joins' => array($join_turma_curso, $join_curso, $join_turma_periodo, $join_periodo)));
                            foreach ($busca_turma as $bt):
                                ?>
                                <tr>
                                    <td><?php echo $bt->nm_turma; ?></td>
                                    <td><?php echo $bt->nm_curso; ?></td>
                                    <td><?php echo $bt->nr_ano . "/" . $bt->nr_periodo ?></td>
                                    <td class="actions-admin"><a data-id-turma="<?php echo $bt->id_turma; ?>" class="btn btn-primary edit-turma" title="Alterar"><i class="btn-icon-only icon-edit"></i></a><a data-id-turma="<?php echo $bt->id_turma; ?>" class="btn btn-warning visualiza-turma" title="Visualizar"><i class="btn-icon-only icon-eye-open"></i></a><a class="btn btn-danger excluir-turma" data-id-turma="<?php echo $bt->id_turma; ?>" title="Excluir"><i class="btn-icon-only icon-remove"></i></a></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>

                    </table>

                </div>

            </div>



            <div id="div-admin-novo">

                <form class="form-horizontal" method="POST" id="form-turma">

                    <div class="control-group">											

                        <label class="control-label" for="nome-turma">Nome da Turma</label>

                        <div class="controls">

                            <input type="text" maxlength="70" class="span5" id="nmTurma" name="nmTurma" placeholder="Nome da Turma">

                        </div> <!-- /controls -->				

                    </div> <!-- /control-group -->

                    <div class="control-group">

                        <label class="control-label" for="periodo">Período:</label>

                        <div class="controls">

                            <select id="cbPeriodo" name="cbPeriodo">
                                <option value="">-- Selecione --</option>
                                <?php
                                $periodo = new app\models\Periodos();
                                $periodo_encontrados = $periodo::all();

                                foreach ($periodo_encontrados as $c):
                                    ?>
                                    <option value="<?php echo $c->id_periodo; ?>"><?php echo utf8_encode($c->nr_ano . "/" . $c->nr_periodo); ?></option>
                                    <?php
                                endforeach;
                                ?>


                            </select>
                        </div>
                    </div><!-- /control-group -->

                    <div class="control-group">

                        <label class="control-label" for="turno">Turno:</label>

                        <div class="controls">

                            <select id="tpTurno" name="tpTurno">
                                <option value="">-- Selecione --</option>
                                <?php
                                $tipo = new app\models\Tipo();
                                $tipo_join = 'INNER JOIN tb_sge_tipo_especializacao te ON(te.id_tipo = tb_sge_tipo.id_tipo)';
                                $tipo_encontrados = $tipo::all(array('select' => '*', 'joins' => $tipo_join, 'conditions' => 'tb_sge_tipo.id_tipo = 2'));

                                foreach ($tipo_encontrados as $e):
                                    ?>
                                    <option value="<?php echo $e->id_especializacao; ?>"><?php echo utf8_encode($e->nm_especializacao); ?></option>
                                    <?php
                                endforeach;
                                ?>


                            </select>
                        </div>
                    </div><!-- /control-group -->

                    <div class="control-group">

                        <label class="control-label" for="curso">Curso:</label>

                        <div class="controls">

                            <select id="cbCurso" name="cbCurso">
                                <option value="">-- Selecione --</option>
                                <?php
                                $curso = new app\models\Curso();
                                $curso_encontrados = $curso::all();

                                foreach ($curso_encontrados as $c):
                                    ?>
                                    <option value="<?php echo $c->id_curso; ?>"><?php echo utf8_encode($c->nm_curso); ?></option>
                                    <?php
                                endforeach;
                                ?>


                            </select>
                        </div>
                    </div><!-- /control-group -->

                    <div class="control-group hide" id="divSemestre">

                        <label class="control-label" for="semestre">Semestres:</label>

                        <div class="controls">

                            <select id="cbSemestre" name="cbSemestre">
                            </select>
                        </div>
                    </div><!-- /control-group -->

                    <div class="control-group">

                        <div class="controls">

                            <input type="submit" class="btn btn-primary" id="btn-cadastrar-turma" value="Cadastrar"/>

                            <input type="submit" class="btn btn-primary hide" id="btn-alterar-turma" value="Alterar"/>

                            <button type="reset" class="btn">Limpar</button>

                            <a class="btn btn-danger" href="?p=turma">Cancelar</a>

                        </div>

                    </div>

                </form>

            </div>

            <div id="div-admin-show">

                <form class="form-horizontal" method="POST">

                    <div class="control-group">                                           

                        <label class="control-label" for="nome">Nome Turma:</label>

                        <div class="controls">
                            <div id="nome" class="margin-top3">
                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->



                    <div class="control-group">                                           

                        <label class="control-label">Periodo:</label>

                        <div class="controls">

                            <div id="periodo" class="margin-top3">

                            </div>

                        </div>    <!-- /controls -->           

                    </div> <!-- /control-group -->



                    <div class="control-group">                                           

                        <label class="control-label" for="turno">Turno:</label>

                        <div class="controls">
                            <div id="turno" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="curso">Curso:</label>

                        <div class="controls">
                            <div id="curso" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                           

                        <label class="control-label" for="semestre">Semestre:</label>

                        <div class="controls">
                            <div id="semestre" class="margin-top3">

                            </div>
                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">

                        <div class="controls">

                            <a class="btn btn-warning" href="?p=turma">Voltar</a>

                        </div>

                    </div>

                </form>

            </div>


            <!-- MODAL EXCLUIR -->

            <div id="modalExcluir" class="modal hide fade in">
                <div class="modal-header">
                    <a type="button" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                    <h3 id="modalTitulo">Excluir Turma</h3>
                </div>
                <div class="modal-body">
                    <p id="msg"></p>
                </div>
                <div class="modal-footer">
                    <button id="btnOk" class="btn hide" data-dismiss="modal" aria-hidden="true">OK</button>
                    <button id="btnExcluirTurma" data-id-turma="" class="btn btn-danger hide">Excluir</button>
                </div>
            </div>

        </div>

    </div>

    <!-- /widget-content -->

</div>