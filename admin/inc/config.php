
<div class="span12">
    <div class="widget widget-table action-table">
        <?php
        if (isset($_SESSION['variavel'])):
            switch ($_SESSION['variavel']) {
                case 'sucesso_alterar_usuario':
                    echo "<div class='alert alert-success'>
                            <a type='button' class='close' data-dismiss='alert'>&times;</a>
                            Alterado com Sucesso !
                          </div>";

                    $_SESSION['variavel'] = '';
                    break;
            }
        endif;
        ?>

        <div class="widget-header"> <i class="icon-th-list"></i>

            <h3>Configurações</h3>

        </div>

        <!-- /widget-header -->

        <div class="widget-content">

            <ul class="nav nav-tabs" role="tablist" id="myTab" style="margin-top: 10px; margin-left: 10px;">

                <li class="active"><a href="#pessoal">Pessoais</a></li>
                <li class=""><a href="#img">Imagem</a></li>

            </ul>

            <div class="tab-content" style="margin-top: 10px; margin-left: 10px;">

                <div id="pessoal" class="tab-pane fade in active">

                    <form class="form-horizontal" method="POST" id="form-perfil">
                        
                        <?php 
                        $usuario = new app\models\Usuarios();
                        
                        $rs_usuario = $usuario::first(array('conditions'=> array('tb_sge_usuario.id_usuario = ?', $_SESSION['usuario_id'])));
                        ?>

                        <div class="control-group">                                           

                            <label class="control-label" for="nome_exibição">Nome de Exibição:</label>

                            <div class="controls">

                                <input type="text" class="span3" name="nome_exibição" id="nome_exibição" maxlength="100" value="<?php echo $rs_usuario->nm_exibicao; ?>" placeholder="Nome de Exibição">

                            </div> <!-- /controls -->               

                        </div> <!-- /control-group -->

                        <div class="control-group">                                           

                            <label class="control-label" for="new_senha">Nova Senha:</label>

                            <div class="controls">

                                <input type="password" class="span2" id="new_senha" name="new_senha"  placeholder="Nova Senha">

                            </div> <!-- /controls -->               

                        </div> <!-- /control-group -->



                        <div class="control-group">                                           

                            <label class="control-label" for="new_senha_r">Repita a Senha:</label>

                            <div class="controls">

                                <input type="password" class="span2" id="new_senha_r" name="new_senha_r"  placeholder="Digite a senha novamente">

                            </div> <!-- /controls -->               

                        </div> <!-- /control-group -->
                        <input type="hidden" name="id_usuario" value="<?php echo $rs_usuario->id_usuario; ?>">

                        <div class="control-group">

                            <div class="controls">

                                <input type="submit" class="btn btn-primary" id="btn-alterar-pessoais" value="Alterar"/>

                                <button type="reset" class="btn">Limpar</button>

                                <a class="btn btn-danger" href="?p=home">Cancelar</a>

                            </div>

                        </div>

                    </form>

                </div>

                <div id="img" class="tab-pane fade">


                    teste

                </div>

            </div>


        </div>

        <!-- /widget-content -->

    </div>
</div>

