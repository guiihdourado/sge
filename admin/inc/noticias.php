<div class="span12">

    <div class="div-btn-novo-admin">

        <button class="btn btn-success" id="bt-admin-novo">Novo <i class="icon-plus"></i></button>

    </div>

    <div class="widget widget-table action-table">

        <div class="widget-header"> <i class="icon-th-list"></i>

            <h3>Notícias</h3>

        </div>

        <!-- /widget-header -->

        <div class="widget-content">
            <div id="div-admin-table">
                <div class="table-responsive padding-table" >

                    <table class="table table-striped table-bordered table-hover" id="dataTables-admin">

                        <thead>

                            <tr>

                                <th>Autor</th>
                                <th>Título</th>

                                <th style="width: 150px; max-width: 150px; min-width: 150px;">Ações</th>

                            </tr>

                        </thead>

                        <tbody>
                            <?php 
                            $noticias = new app\models\Noticias();
                            $noticias_encontradas = $noticias->listar();
                            
                            foreach ($noticias_encontradas as $ne):
                            ?>

                            <tr>

                                <td><?php echo $ne->nm_autor; ?></td>
                                <td><?php echo $ne->nm_titulo; ?></td>
                                <td class="actions-admin"><a href="javascript:;" class="btn btn-primary" title="Editar"><i class="btn-icon-only icon-edit"> </i></a><a href="javascript:;" class="btn btn-warning" title="Visualizar"><i class="btn-icon-only icon-eye-open"></i></a><a href="javascript:;" class="btn btn-danger" title="Excluir"><i class="btn-icon-only icon-remove"> </i></a></td>

                            </tr>
                            
                            <?php endforeach; ?>

                        </tbody>

                    </table>

                </div>
            </div>
            <div id="div-admin-novo">

                <form class="form-horizontal" method="POST" id="form-noticia">

                        <div class="control-group">											

                            <label class="control-label" for="autor">Autor</label>

                            <div class="controls">

                                <input type="text" class="span5" id="autor" name="autor"  placeholder="Autor">

                            </div> <!-- /controls -->				

                        </div> <!-- /control-group -->


                        <div class="control-group">											

                            <label class="control-label" for="titulo">Titulo</label>

                            <div class="controls">

                                <input type="text" class="span5" id="titulo" name="titulo"  placeholder="Titulo">

                            </div> <!-- /controls -->				

                        </div> <!-- /control-group -->

                        <div class="control-group">											

                            <label class="control-label" for="mensagem">Mensagem</label>

                            <div class="controls">

                                <textarea id="mensagem-editor" class="span5" name="mensagem"  placeholder="Mensagem"></textarea>

                            </div> <!-- /controls -->

                        </div> <!-- /control-group -->

                        <div class="control-group">

                            <div class="controls">

                                <button type="submit" class="btn btn-primary" id="btn-cadastrar-noticia">Enviar</button> 

                                <button type="reset" class="btn">Limpar</button>

                                <button class="btn btn-danger" id="bt-admin-fechar">Cancelar</button>

                            </div>

                        </div>

                    </form>

                </div>
        </div>


    </div>

    <!-- /widget-content --> 

</div>