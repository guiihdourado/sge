$(document).ready(function () {

    //Variaveis
    var div_admin_novo = $("#div-admin-novo");
    
    var btn_cadastrar_noticia = div_admin_novo.find('#btn-cadastrar-noticia');
    var form_noticia = div_admin_novo.find('#form-noticia');
    
    $("#bt-admin-novo").on('click', function () {
        $("#div-admin-table").fadeOut('slow');
        $("#div-admin-novo").fadeIn(('slow'));
    });

    btn_cadastrar_noticia.on('click', function () {
        $.ajax({
            url: 'http://sge.com.br/ajax/cadastrar_noticia.php',
            type: 'post',
            data: form_noticia.serialize(),
            beforeSend: function () {

            },
            success: function (retorno) {
                alert(retorno);
                window.location.reload();
            }

        });
        return false;
    }); 
});