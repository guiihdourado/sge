$(document).ready(function(){
    var div_pessoal = $("#pessoal");
    var form_perfil = div_pessoal.find("#form-perfil");
    
    form_perfil.validate({
        rules:{
            new_senha: {minlength: 6},
            new_senha_r: {minlength: 6, equalTo: "#new_senha"},
        },
        messages: {
            new_senha: {minlength: 'Informe no mínimo 6 dígitos'},
            new_senha_r: {minlenght: 'Informe no mínimo 6 dígitos', equalTo: 'As senhas não coincidem.'}
        },
        submitHandler: function(form){
            $.ajax({
                url: 'http://sge.com.br/ajax/atualizar_usuario.php',
                type: 'post',
                data: form_perfil.serialize(),
                beforeSend: function(){
                    
                },
                success: function(retorno){
                   console.log(retorno);
                }
            });
        }
    });
    
    
    
    
    
});