$(document).ready(function () {
    var div_admin_novo = $("#div-admin-novo");
    var form_disciplina = div_admin_novo.find('#form_disciplina');

    form_disciplina.validate({
        rules: {
            disc: {required: true, minlength: 3, maxlength: 100},
            grade: {required: true},
            falta: {required: true},
            media: {required: true},
            select_prof: {required: true}
        },
        messages: {
            disc: {required: 'Campo obrigatório', minlength: "Informe no mínimo 3 caracteres", maxlength: "Informe no máximo 100 caracteres"},
            grade: {required: 'Campo obrigatório'},
            falta: {required: 'Campo obrigatório'},
            media: {required: 'Campo obrigatório'},
            select_prof: {required: 'Campo obrigatório'}
        }
    });

    Disciplina();

});

/*Função Carregada na Página de Disciplina*/
function Disciplina() {
    var nome = $("#disc");
    var grade = $("#grade");
    var falta = $("#falta");
    var media = $("#media");

    nome.alpha({allow: " "});
    grade.numeric();
    falta.numeric();
    media.priceFormat({
        prefix: '',
        centsSeparator: ',',
        limit: 4,
        centsLimit: 2,
        clearPrefix: true
    });
    media.blur(function () {
        if (parseFloat($(this).val().replace(",", ".")) > 10.00 || $(this).val() === "") {
            $(this).val("");
        }
    });

    $('input[type=radio][name=rbDependente]').change(function () {
        if (this.value === '1') {
            $("#select_disc").rules("add", {
                required: true,
                messages: {
                    required: "Campo obrigatório"
                }
            });
            $("#div-disc-dependente").fadeIn('slow').removeClass("hide");
        }
        else {
            $("#select_disc").rules("remove");
            $("#div-disc-dependente").fadeOut('slow').addClass("hide");
        }
    });

    $("#btn-cadastrar-disciplina").click(function () {
        if ($('#form_disciplina').valid()) {
            $.ajax({
                url: 'http://sge.com.br/ajax/cadastrar_disciplina.php',
                type: 'post',
                data: $("#form_disciplina").serialize(),
                beforeSend: function () {

                },
                success: function (retorno) {
                    window.location.reload();
                }

            });
            return false;
        }
    });

    $(".edit-disciplina").click(function () {
        var id = $(this).attr("data-id-disciplina");
        $.ajax({
            url: 'http://sge.com.br/ajax/editar_disciplina.php',
            type: 'post',
            data: {id: id, tipo: 'busca'},
            dataType: 'json',
            success: function (retorno) {
                var obj = $.parseJSON(retorno);
                $("#disc").val(obj.nm_disciplina);
                $("#grade").val(obj.nr_carga_horaria);
                $("#falta").val(obj.nr_percentual_faltas);
                $("#media").val(obj.nr_media_minima);
                var input = "<input type='hidden'id='id_disciplina' name='id_disciplina' value='" + id + "'>";
                $("#form-disciplina").append(input);
                $("#select_prof").val(obj.id_professor);
                if (obj.id_disciplina_depende !== null) {
                    $('input[type=radio][name=rbDependente][value=1]').attr("checked", "cheked");
                    $('input[type=radio][name=rbDependente]').change();
                    $('#select_disc').val(obj.id_disciplina_depende);
                }
                $("#btn-cadastrar-disciplina").addClass("hide");
                $("#btn-alterar-disciplina").removeClass("hide");
                $("#div-admin-table").fadeOut('slow');
                $("#div-admin-novo").fadeIn(('slow'));
            },
            error: function (e) {
                alert(e);
            }
        });
        return false;
    });
    
    $("#btn-alterar-disciplina").click(function(){
        if ($('#form_disciplina').valid()) {
            $.ajax({
                url: 'http://sge.com.br/ajax/editar_disciplina.php',
                type: 'post',
                data: $('#form_disciplina').serialize(),
                dataType: 'json',
                success: function (retorno) {
                    window.location.reload();
                },
                error:function(e){
                    alert(e);
                }
            });
            return false;
        }
    });
    
    $(".excluir-disciplina").click(function () {
        var id = $(this).attr("data-id-disciplina");
        $.ajax({
            url: 'http://sge.com.br/ajax/excluir_disciplina.php',
            type: 'post',
            data: {id: id, tipo: "validar"},
            dataType: 'json',
            success: function (retorno) {
                if (retorno.sucesso) {
                    $("#msg").html("Deseja realmente excluir?");
                    $("#btnExcluirTurma").removeClass("hide");
                    $("#btnOk").addClass("hide");
                    $("#modalExcluir").modal('show');
                    $("#btnExcluirDisciplina").click(function () {
                        ExcluirDisciplina(id);
                    });
                } else {
                    $("#msg").html("Não é possivel excluir! Existe " + retorno.vinculo + " Vinculados!");
                    $("#btnExcluirDisciplina").addClass("hide");
                    $("#btnOk").removeClass("hide");
                    $("#modalExcluir").modal('show');
                }
            }
        });
        return false;
    });
}

function ExcluirDisciplina(id){
    $.ajax({
        url: 'http://sge.com.br/ajax/excluir_disciplina.php',
        type: 'post',
        data: {id: id},
        dataType: 'json',
        success: function (retorno) {
            window.location.reload();
        },
        error: function (e) {
            alert(e);
        }
    });
}