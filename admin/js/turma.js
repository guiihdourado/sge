var _sem;
$(document).ready(function () {
    Validacoes();
    Eventos();
});

function Validacoes() {
    $("#form-turma").validate({
        rules: {
            nmTurma: {required: true},
            cbPeriodo: {required: true},
            cbCurso: {required: true},
            tpTurno: {required: true},
            cbSemestre: {required: true}
        },
        messages: {
            nmTurma: {required: "Campo Obrigatório"},
            cbPeriodo: {required: "Campo Obrigatório"},
            cbCurso: {required: "Campo Obrigatório"},
            tpTurno: {required: "Campo Obrigatório"},
            cbSemestre: {required: "Campo Obrigatório"}
        }
    })
}

function Eventos()
{
    $("#cbCurso").change(function (e) {
        e.preventDefault();
        if ($(this).val() !== "") {
            $.ajax({
                url: 'http://sge.com.br/ajax/listar_semestre.php',
                type: 'post',
                data: {'idCurso': $(this).val()},
                beforeSend: function () {

                },
                success: function (retorno) {
                    var obj = $.parseJSON(retorno);
                    var html = "<option>-- Selecione --</option>";
                    for (x = 1; x <= parseInt(obj[0].nr); x++)
                    {
                        if (_sem === x) {
                            html += "<option selected value='" + x + "'>" + x + "</option>";
                        }
                        else {
                            html += "<option value='" + x + "'>" + x + "</option>";

                        }
                    }
                    $("#cbSemestre").html(html);
                    $("#divSemestre").removeClass("hide");
                }

            });
        } else {
            $("#cbSemestre").html("");
            $("#divSemestre").addClass("hide");
        }
    });

    $("#btn-cadastrar-turma").click(function () {
        if ($('#form-turma').valid()) {
            $.ajax({
                url: 'http://sge.com.br/ajax/cadastrar_turma.php',
                type: 'post',
                data: $('#form-turma').serialize(),
                beforeSend: function () {

                },
                success: function (retorno) {
                    window.location.reload();
                }

            });
        }
    });

    $(".edit-turma").click(function () {
        var id = $(this).attr("data-id-turma");
        $.ajax({
            url: 'http://sge.com.br/ajax/editar_turma.php',
            type: 'post',
            data: {id: id, tipo: 'busca'},
            dataType: 'json',
            success: function (retorno) {
                var obj = $.parseJSON(retorno);
                _sem = parseInt(obj.nr_semestre);
                $("#nmTurma").val(obj.nm_turma);
                $("#cbPeriodo").val(obj.id_periodo);
                $("#tpTurno").val(obj.tp_turno);
                $("#cbCurso").val(obj.id_curso);
                $("#cbCurso").change();
                var input = "<input type='hidden'id='id_turma' name='id_turma' value='" + id + "'>";
                $("#form-turma").append(input);

                $("#btn-cadastrar-turma").addClass("hide");
                $("#btn-alterar-turma").removeClass("hide");
                $('#cbSemestre option[id=' + _sem + ']').attr("selected", "selected");
                $("#div-admin-table").fadeOut('slow');
                $("#div-admin-novo").fadeIn(('slow'));
            }
        });
        return false;
    });

    $("#btn-alterar-turma").click(function () {
        if ($('#form-turma').valid()) {
            $.ajax({
                url: 'http://sge.com.br/ajax/editar_turma.php',
                type: 'post',
                data: $('#form-turma').serialize(),
                dataType: 'json',
                success: function (retorno) {
                    window.location.reload();
                }
            });
            return false;
        }
    });

    $(".visualiza-turma").click(function () {
        var id = $(this).attr("data-id-turma");
        $.ajax({
            url: 'http://sge.com.br/ajax/visualizar_turma.php',
            type: 'post',
            data: {id: id},
            dataType: 'json',
            success: function (retorno) {
                var obj = $.parseJSON(retorno);
                $("#nome").html(obj.nm_turma);
                $("#periodo").html(obj.nr_ano + "/" + obj.nr_periodo);
                $("#tpTurno").val(obj.tp_turno);
                $("#turno").html($("#tpTurno option:selected").text());
                $("#cbCurso").val(obj.id_curso);
                $("#curso").html($("#cbCurso option:selected").text());
                $("#semestre").html(obj.nr_semestre);

                $("#div-admin-table").fadeOut('slow');
                $("#div-admin-show").fadeIn(('slow'));
            }
        });
        return false;
    });

    $(".excluir-turma").click(function () {
        var id = $(this).attr("data-id-turma");
        $.ajax({
            url: 'http://sge.com.br/ajax/excluir_turma.php',
            type: 'post',
            data: {id: id, tipo: "validar"},
            dataType: 'json',
            success: function (retorno) {
                if (retorno.sucesso) {
                    $("#msg").html("Deseja realmente excluir?");
                    $("#btnExcluirTurma").removeClass("hide");
                    $("#btnOk").addClass("hide");
                    $("#modalExcluir").modal('show');
                    $("#btnExcluirTurma").click(function () {
                        ExcluirTurma(id);
                    });
                } else {
                    $("#msg").html("Não é possivel excluir! Existe " + retorno.vinculo + " Vinculados!");
                    $("#btnExcluirTurma").addClass("hide");
                    $("#btnOk").removeClass("hide");
                    $("#modalExcluir").modal('show');
                }
            }
        });
        return false;
    });
}

function ExcluirTurma(id)
{
    $.ajax({
        url: 'http://sge.com.br/ajax/excluir_turma.php',
        type: 'post',
        data: {id: id},
        dataType: 'json',
        success: function (retorno) {
            window.location.reload();
        },
        error: function (e) {
            alert(e);
        }
    });
}