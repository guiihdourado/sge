var _nrSemestres;
var _dados = new Array();
var disciplina = new Object();
$(document).ready(function () {
    var div_admin_table = $("#div-admin-table");
    var div_admin_novo = $("#div-admin-novo");
    var btn_cadastrar_curso = $("#btnCadastrarCurso");
    var form_curso = div_admin_novo.find('#form_curso');
    var btn_alterar_curso = $("#btnAlterarCurso");

    var btn_editar_curso = div_admin_table.find('#btn-editar-curso');

    $("#bt-admin-novo").on('click', function () {
        btn_cadastrar_curso.removeClass("hide");
        btn_alterar_curso.addClass("hide");
        $("#div-admin-table").fadeOut('slow');
        $("#div-admin-novo").fadeIn(('slow'));
    });

    form_curso.validate({
        rules: {
            nmCurso: {required: true, minlength: 3, maxlength: 70},
            descricaoCurso: {required: true},
            cargaCurso: {required: true},
            tpCurso: {required: true},
            qtdSem: {required: true}
        },
        messages: {
            nmCurso: {required: 'Campo obrigatório', minlength: "Informe no mínimo 3 caracteres", maxlength: "Informe no máximo 70 caracteres"},
            descricaoCurso: {required: 'Campo obrigatório'},
            cargaCurso: {required: 'Campo obrigatório'},
            tpCurso: {required: 'Campo obrigatório'},
            qtdSem: {required: 'Campo obrigatório'}
        }
    });

    btn_editar_curso.on('click', function () {

        var id = $(this).attr('data-id-curso');

        $.ajax({
            url: 'http://sge.com.br/ajax/editar_curso.php',
            type: 'POST',
            data: {id: id, tipo: 'busca'},
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (retorno) {
                var obj = $.parseJSON(retorno.curso);
                btn_cadastrar_curso.addClass("hide");
                btn_alterar_curso.removeClass("hide");
                btn_alterar_curso.removeAttr("disabled");
                var input = "<input type='hidden' name='id_curso' value='" + id + "'>";
                $('#form_curso').append(input);

                $("#nmCurso").val(obj.nm_curso);
                $("#descricaoCurso").val(obj.ds_descricao);
                $("#cargaCurso").val(obj.nr_carga_horaria);
                $("#tpCurso").val(obj.tp_area);
                $("#qtdSem").val(obj.nr_semestre);

                GerarInsercaoSemestre();
                var i;
                for (i in retorno.disciplinas) {
                    $("#select" + retorno.disciplinas[i].semestre).val(retorno.disciplinas[i].id);
                    $("#select" + retorno.disciplinas[i].semestre).change();
                }
                $("#div-admin-table").fadeOut('slow');
                $("#div-admin-novo").fadeIn(('slow'));
            },
            error: function (e) {
                alert(e);
            }
        });
        return false;
    });

    Curso();
});

function Curso() {
    var nmCurso = $("#nmCurso");
    var cargaCurso = $("#cargaCurso");
    var qtdSem = $("#qtdSem");

    nmCurso.alpha({allow: " "});
    cargaCurso.numeric();
    qtdSem.numeric();

    $("#btn_qnt_semestre").click(function () {
        $("#img-loader").fadeIn("slow").show();
        GerarInsercaoSemestre();
        $("#btnCadastrarCurso").removeAttr("disabled");
        $("#btnAlterarCurso").removeAttr("disabled");
    });
    $("#btnCadastrarCurso").click(function (e) {
        if ($('#form_curso').valid()) {
            PreencherObjDisciplina();
            $.ajax({
                url: 'http://sge.com.br/ajax/cadastrar_curso.php',
                type: 'post',
                async: true,
                data: {'formulario': $('#form_curso').serialize(), 'objDisc': JSON.stringify(_dados)},
                beforeSend: function () {

                },
                success: function (retorno) {
                    window.location.reload();
                }

            });
        }
    });

    $("#btnAlterarCurso").click(function () {
        if ($('#form_curso').valid()) {
            PreencherObjDisciplina();
            $.ajax({
                url: 'http://sge.com.br/ajax/editar_curso.php',
                type: 'post',
                async: true,
                data: {'formulario': $('#form_curso').serialize(), 'objDisc': JSON.stringify(_dados)},
                beforeSend: function () {

                },
                success: function (retorno) {
                    window.location.reload();
                }
            });
        }
    });
}

function GerarInsercaoSemestre() {
    $("#myTab").html("");
    $("#tab-Content-Abas").html("");
    var nrSemestre = $("#qtdSem").val();
    _nrSemestres = nrSemestre;
    var i;
    for (i = 1; i <= parseInt(nrSemestre); i++) {
        var li = document.createElement("li");
        var html = "<a href='#" + i + "'>" + i + "º Semestre</a>";
        (i === 1) ? li.className = "active" : li.className = "";
        $(li).append(html);

        var divAba = document.createElement("div");
        divAba.className = (i === 1) ? "tab-pane fade in active" : "tab-pane fade";
        divAba.id = i;
        var select = "<div class='control-group'><div class='controls'>";
        select += "<select class='selectDisciplina span6' id='select" + i + "' data-id='" + i + "'></select><img class='hide imgLoad' id='img-loader-select" + i + "' src='../../public/img/ajax-loader.gif'></div></div>";
        var table = MontarTabela(i);
        $(divAba).html(select);
        $(divAba).append(table);

        $("#myTab").append(li);
        $("#tab-Content-Abas").append(divAba);

        AddValidacaoDisciplina("#select" + i);

        $("#myTab a").click(function (e) {

            e.preventDefault();

            $(this).tab('show');

        });
    }
    $("#div-abas-semestre").fadeIn("slow");
    $.ajax({
        type: "POST",
        url: 'http://sge.com.br/ajax/listar_disciplinas.php',
        dataType: "json",
        async: false,
        success: function (retorno) {
            var option = "";
            if (retorno.length > 0) {
                option += "<option value=''>-- Selecione --</option>";

                $(retorno).each(function (i, e) {
                    option += "<option value='" + e.id + "'>" + e.nome + "</option>";
                });
            }
            else {
                option += "<option value=''>Registo não Encontrado</option>";
            }
            $(".selectDisciplina").html(option);
            $("#content_abas").fadeIn("slow").removeClass("hide");
            $("#img-loader").fadeOut("slow").hide();
        }
    });

    $(".selectDisciplina").change(function () {
        var indice = $(this).attr("data-id");
        $("#img-loader-select" + indice).fadeIn('slow').removeClass("hide");
        CarregarDisciplinasSemestre(this, $(this).attr("data-id"));
        if ($("#tabela" + $(this).attr("data-id") + " tbody tr").length < 2)
            AddValidacaoDisciplina("#select" + indice);
        else
            RemoveValidacaoDisciplina("#select" + indice);
    });
}

function MontarTabela(aba) {
    var sOut = "<table id='tabela" + aba + "' class='table table-bordered table-hover'>";
    sOut += "<thead>";
    sOut += "<tr>";
    sOut += "<th>Nome</th>";
    sOut += "<th>Carga Horária</th>";
    sOut += "<th>Média Minima</th>";
    sOut += "<th style='width: 68px;'>Ação</th>";
    sOut += "</tr>";
    sOut += "</thead>";
    sOut += "<tbody id='corpoTabela" + aba + "'>";
    sOut += "</tbody>";
    sOut += "</table>";

    return sOut;
}

function CarregarDisciplinasSemestre(select, aba) {

    $.ajax({
        type: "POST",
        url: 'http://sge.com.br/ajax/selecionar_disciplina.php',
        data: {"id": $(select).val()},
        dataType: 'json',
        async: false,
        success: function (retorno) {
            retorno = $.parseJSON(retorno);
            var tamanho = $("#tabela" + aba + " tbody tr").length;
            if (tamanho === 10) {
                return;
            }
            var indiceLista = ProximoIndiceTabela("#tabela" + aba);

            var nome = retorno.nm_disciplina;
            var carga = retorno.nr_carga_horaria;
            var media = retorno.nr_media_minima;
            var id = retorno.id_disciplina;
            var botaoExcluir = "<a class='center-td remover' href=javascript:RemoverItem(" + indiceLista + ")><i class='color-red-darker icon-remove icon-large'></i></a>";
            var linha = "<tr id='linha_" + indiceLista + "' data-id='" + id + "'><td data-nome='" + nome + "'>" + nome + "</td><td>" + carga + "</td><td>" + media + "</td><td class='text-center'>" + botaoExcluir + "</td></tr>";
            $("#tabela" + aba).append(linha);
            $(".selectDisciplina option[value=" + $(select).val() + "]").remove();
            $("#img-loader-select" + aba).fadeOut('slow').addClass("hide");
        },
        error: function (e) {
            alert(e.status);
        }
    });
}

function ProximoIndiceTabela(tabela) {
    var indiceLista = $(tabela + " tbody tr:last").prop("id");
    return (indiceLista === undefined) ? 0 : parseInt(indiceLista.split("_")[1]) + 1;
}

function PreencherObjDisciplina() {
    _dados = new Array();
    for (x = 1; x <= _nrSemestres; x++) {
        _dados[x - 1] = new Array();
        if ($("#tabela" + x + " tbody tr").length < 2)
        {
            return false;
        }
        else {
            $("#tabela" + x + " tbody tr").each(function (indice, i) {
                disciplina = new Object();
                var id = $(this).attr("data-id");
                var semestre = x;
                disciplina.id = id;
                disciplina.semestre = semestre;
                _dados[x - 1][indice] = disciplina;
            });
        }
    }
}

function RemoverItem(indice) {
    var nome = $("#linha_" + indice).find("td[data-nome]").text();
    var id = $("#linha_" + indice).attr("data-id");
    $(".selectDisciplina").append("<option value='" + id + "'>" + nome + "</option>");
    $("#linha_" + indice).remove();
}

function AddValidacaoDisciplina(id) {
    $(id).rules("add", {
        required: true,
        messages: {
            required: "Campo Obrigatório!"
        }
    });
}

function RemoveValidacaoDisciplina(id) {
    $(id).rules("remove");
}