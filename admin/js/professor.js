$(document).ready(function () {

    //Variaveis
    var div_admin_novo = $("#div-admin-novo");
    var form_professor = div_admin_novo.find('#form-professor');
    var div_admin_table = $("#div-admin-table");
    var nome = div_admin_novo.find("#nome_completo");
    var sexo = div_admin_novo.find("#sexo");
    var email = div_admin_novo.find('#email');
    var nome_mae = div_admin_novo.find('#nome_mae');
    var nome_pai = div_admin_novo.find('#nome_pai');
    var cpf = div_admin_novo.find("#cpf");
    var rg = div_admin_novo.find("#rg");
    var endereco = div_admin_novo.find('#endereco');
    var telefone = div_admin_novo.find('#telefone');
    var celular = div_admin_novo.find('#celular');
    var cep = div_admin_novo.find('#cep');
    var bairro = div_admin_novo.find('#bairro');
    var estado = div_admin_novo.find('#estado');
    var cidade = div_admin_novo.find('#cidade');
    var dt_nasc = div_admin_novo.find('#dt_nasc');
    var curso = div_admin_novo.find('#curso');
    var select_estado = div_admin_novo.find('#estado');
    var img_loader = div_admin_novo.find('#img-loader');
    var btn_cadastrar_professor = div_admin_novo.find("#btn-cadastrar-professor");
    var cidade_div = div_admin_novo.find('#cidade-div');

    var btn_editar_professor = div_admin_table.find('#btn-editar-professor');
    var btn_visualizar_professor = div_admin_table.find('#btn-visualizar-professor');

    $("#nome_completo").alpha({allow: " "});
    $("#nome_pai").alpha({allow: " "});
    $("#nome_mae").alpha({allow: " "});
    $("#telefone").numeric();
    $("#celular").numeric();
    $("#cep").numeric();

    form_professor.validate({
        rules: {
            nome_completo: {required: true, minlength: 3, maxlength: 100},
            cpf: {required: true, cpf: true, minlength: 14, maxlength: 14},
            sexo: {required: true},
            rg: {required: true, minlength: 9, maxlength: 9},
            dt_nasc: {required: true},
            nome_pai: {required: true},
            nome_mae: {required: true},
            telefone: {required: true, minlength: 13, maxlength: 13},
            celular: {required: true, minlength: 13, maxlength: 13},
            email: {required: true, email: true, maxlength: 70},
            endereco: {required: true},
            bairro: {required: true},
            cidade: {required: true},
            cep: {required: true, minlength: 9, maxlength: 9}
        },
        messages: {
            nome_completo: {required: 'Campo obrigatório.', minlength: 'Informe no minimo 3 dígitos.', maxlength: 'Tamanho máximo é de 100 caracteres.'},
            cpf: {required: 'Campo obrigatório.', minlength: 'Informe todos os digitos do CPF.', maxlength: 'Informe todos os digitos do CPF.'},
            sexo: {required: 'Campo obrigatório.'},
            rg: {required: 'Campo obrigatório.', minlength: 'Informe todos os digitos do RG.', maxlength: 'Informe todos os digitos do RG.'},
            dt_nasc: {required: 'Campo obrigatório.'},
            nome_pai: {required: 'Campo obrigatório.'},
            nome_mae: {required: 'Campo obrigatório.'},
            telefone: {required: 'Campo obrigatório.', minlength: 'Informe todos os digitos do telefone.', maxlength: 'Informe todos os digitos do telefone.'},
            celular: {required: 'Campo obrigatório.', minlength: 'Informe todos os digitos do celular.', maxlength: 'Informe todos os digitos do celular.'},
            email: {required: 'Campo obrigatório.', email: 'Digite um email válido', maxlength: 'Tamanho máximo é de 70 caracteres'},
            endereco: {required: 'Campo obrigatório.'},
            bairro: {required: 'Campo obrigatório.'},
            cidade: {required: 'Campo obrigatório.'},
            cep: {required: 'Campo obrigatório.', minlength: 'Informe todos os digitos do cep.', maxlength: 'Informe todos os digitos do cep.'}
        },
        submitHandler: function (form) {
            $.ajax({
                url: 'http://sge.com.br/ajax/cadastrar_professor.php',
                type: 'post',
                data: form_professor.serialize(),
                beforeSend: function () {

                },
                success: function (retorno) {
                    window.location.reload();
                }

            });
            return false;
        }
    });

    btn_editar_professor.on('click', function () {

        var id = $(this).attr('data-id-professor');

        $.ajax({
            url: 'http://sge.com.br/ajax/editar_professor.php',
            type: 'post',
            data: {id: id, tipo: 'busca'},
            dataType: 'json',
            beforeSend: function () {
                btn_cadastrar_professor.attr('id', 'btn-alterar-professor');
                form_professor.attr('id', 'form-alterar-professor');
            },
            success: function (retorno) {
                var obj = $.parseJSON(retorno);
                if (obj.tp_sexo == 1) {
                    document.getElementById("masculino").checked = true;
                } else if (obj.tp_sexo == 2) {
                    document.getElementById("feminino").checked = true;
                }

                var btn_alterar_professor = div_admin_novo.find("#btn-alterar-professor");
                var form_alterar_professor = div_admin_novo.find('#form-alterar-professor');

                var input = "<input type='hidden' name='id_professor' value='" + id + "'>";
                form_alterar_professor.append(input);


                nome.val(obj.nm_pessoa);
                cpf.val(mascaraCpf(obj.nr_cpf));
                dt_nasc.val(obj.dt_nascimento);
                email.val(obj.ds_email);
                rg.val(mascaraRg(obj.nr_rg));
                celular.val(obj.nr_celular);
                telefone.val(obj.nr_telefone);
                nome_mae.val(obj.nm_mae);
                nome_pai.val(obj.nm_pai);
                endereco.val(obj.nm_endereco);
                bairro.val(obj.nm_bairro);
                estado.val(obj.id_estado);
                cidade_div.html("<select id='cidade' name='cidade'><option value=" + obj.id_cidade + " name='teste'>" + obj.nome + "</option></select>");
                cep.val(obj.nr_cep);
                btn_cadastrar_professor.val('Alterar');
                $("#div-admin-table").fadeOut('slow');
                $("#div-admin-novo").fadeIn(('slow'));


                btn_alterar_professor.on('click', function () {
                    $.ajax({
                        url: 'http://sge.com.br/ajax/editar_professor.php',
                        type: 'post',
                        data: form_alterar_professor.serialize(),
                        beforeSend: function () {

                        },
                        success: function (retorno) {
                        }
                    });
                });

            }
        });
        return false;
    });

    btn_visualizar_professor.on('click', function () {
        var id = $(this).attr('data-id-professor');
        $.ajax({
            url: 'http://sge.com.br/ajax/visualizar_professor.php',
            type: 'post',
            data: {id: id},
            dataType: 'json',
            beforeSend: function () {

            },
            success: function (retorno) {
                var div_admin_show = $("#div-admin-show");
                var nome = div_admin_show.find('#nome_completo');
                var sexo = div_admin_show.find('#sexo');
                var cpf = div_admin_show.find('#cpf');
                var rg = div_admin_show.find('#rg');
                var dt_nasc = div_admin_show.find('#dt_nasc');
                var nm_pai = div_admin_show.find('#nm_pai');
                var nm_mae = div_admin_show.find('#nm_mae');
                var telefone = div_admin_show.find('#telefone');
                var celular = div_admin_show.find('#celular');
                var email = div_admin_show.find('#email');
                var endereco = div_admin_show.find('#endereco');
                var bairro = div_admin_show.find('#bairro');
                var estado = div_admin_show.find('#estado');
                var cidade = div_admin_show.find('#cidade');
                var cep = div_admin_show.find('#cep');
                var matricula = div_admin_show.find('#matricula');
                var obj = $.parseJSON(retorno);

                nome.html(obj.nm_pessoa);
                if (obj.tp_sexo == 1) {
                    sexo.html('Masculino');
                } else if (obj.tp_sexo == 2) {
                    sexo.html('Feminino');
                }
                cpf.html(mascaraCpf(obj.nr_cpf));
                rg.html(mascaraRg(obj.nr_rg));
                dt_nasc.html(obj.dt_nascimento);
                nm_pai.html(obj.nm_pai);
                nm_mae.html(obj.nm_mae);
                telefone.html(obj.nr_telefone);
                celular.html(obj.nr_celular);
                email.html(obj.ds_email);
                endereco.html(obj.nm_endereco);
                bairro.html(obj.nm_bairro);
                estado.html(obj.uf);
                cidade.html(obj.nome);
                cep.html(obj.nr_cep);
                curso.html(obj.nm_curso);
                matricula.html(obj.nr_matricula);

                $("#div-admin-table").fadeOut('slow');
                $("#div-admin-show").fadeIn(('slow'));
            }
        });
    });

    $(".excluir_professor").click(function () {
        var id = $(this).attr('data-id-professor');
        $("#msg").html("Deseja realmente excluir?");
        $("#modalExcluir").modal('show');
        $("#btnExcluirProfessor").click(function () {
            ExcluirProfessor(id);
        });
    });

    function ExcluirProfessor(id) {
        $.ajax({
            url: 'http://sge.com.br/ajax/excluir_professor.php',
            type: 'post',
            data: {id: id},
            beforeSend: function () {

            },
            success: function (retorno) {
                window.location.reload();
            }
        });
        return false;
    }

    function mascaraCpf(cpf) {

        retorno = cpf.split('');

        var i = 0;
        var cpf2 = '';

        while (i < retorno.length) {
            cpf2 += retorno[i];
            if (i == 2 || i == 5) {
                cpf2 += '.';
            }
            if (i == 8) {
                cpf2 += '-';
            }
            i++;
        }
        return cpf2;
    }

    function mascaraRg(rg) {

        retorno = rg.split('');

        var i = 0;
        var rg2 = '';

        while (i < retorno.length) {
            rg2 += retorno[i];
            if (i == 0 || i == 3) {
                rg2 += '.';
            }
            i++;
        }

        return rg2;
    }
});

function ReplaceAll(Source, stringToFind, stringToReplace) {

    var temp = Source;
    var index = temp.indexOf(stringToFind);
    while (index != -1) {
        temp = temp.replace(stringToFind, stringToReplace);
        index = temp.indexOf(stringToFind);
    }

    return temp;
}

function ValidarCPF(valor) {
    value = jQuery.trim(valor);

    value = value.replace('.', '');
    value = value.replace('.', '');
    cpf = value.replace('-', '');
    if (cpf == "12345678909")
        return false;
    while (cpf.length < 11)
        cpf = "0" + cpf;
    var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
    var a = [];
    var b = new Number;
    var c = 11;
    for (i = 0; i < 11; i++) {
        a[i] = cpf.charAt(i);
        if (i < 9)
            b += (a[i] * --c);
    }
    if ((x = b % 11) < 2) {
        a[9] = 0
    } else {
        a[9] = 11 - x
    }
    b = 0;
    c = 11;
    for (y = 0; y < 10; y++)
        b += (a[y] * c--);
    if ((x = b % 11) < 2) {
        a[10] = 0;
    } else {
        a[10] = 11 - x;
    }
    if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg))
        return false;
    return true;
}

jQuery.validator.addMethod("cpf", function (value, element) {
    var cpf = ReplaceAll(value, "_", "").replace("-", "");
    if (ReplaceAll(cpf, ".", "").length == 0)
        return true;
    if (value.length == 14)
        return ValidarCPF(value) || element.required;
}, "Informe um CPF válido.");