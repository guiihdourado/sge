$(document).ready(function () {
    var myModal = $("#myModal");
    var form_eventos = myModal.find("#form-eventos");

    form_eventos.validate({
        rules: {
            titulo: {required: true},
            dia: {required: true},
            hora: {required: true},
            descricao: {required: true}
        },
        messages: {
            titulo: {required: "Campo Obrigatório."},
            dia: {required: "Campo Obrigatório."},
            hora: {required: "Campo Obrigatório."},
            descricao: {required: "Campo Obrigatório."}
        },
        submitHandler: function(form){
            $.ajax({
                url: 'http://sge.com.br/ajax/cadastrar_eventos.php',
                type: 'POST',
                data: form_eventos.serialize(),
                beforeSend: function(){
                    
                },
                success: function(retorno){
                    alert(retorno);
                    window.location.reload();
                }
            });
        }
    });
});

