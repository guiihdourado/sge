<?php

function gera_matricula_aluno() {
    $ano = (int) Date('Y');
    $novo_valor = "";
    $valor = "0123456789";
    for ($i = 0; $i < 5; $i++) {
        $novo_valor.= $valor[mt_rand() % strlen($valor)];
    }
    return $ano .'1'.$novo_valor;
}

function gera_matricula_professor() {
    $ano = (int) Date('Y');
    $novo_valor = "";
    $valor = "0123456789";
    for ($i = 0; $i < 5; $i++) {
        $novo_valor.= $valor[mt_rand() % strlen($valor)];
    }
    return $ano .'2'.$novo_valor;
}
