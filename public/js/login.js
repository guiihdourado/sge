$(document).ready(function () {

    //Variaveis    
    var div_id_login = $("#id-login");
    var div_id_signup = $("#id-signup");
    var matricula_primeiro_acesso = div_id_signup.find("#matricula");
    var matricula_login = div_id_login.find("#matricula")
    var form_primeiro_acesso = div_id_signup.find("#form-primeiro-acesso");
    var form_login = div_id_login.find("#form-login");

    matricula_primeiro_acesso.mask('0000000000');

    form_primeiro_acesso.validate({
        rules: {
            matricula: {required: true, minlength: 10, maxlength: 10},
            senha: {required: true, minlength: 6},
            confirme_senha: {required: true, minlength: 6},
            Field: {required: true}
        },
        messages: {
            matricula: {required: 'Campo obrigatório.', minlength: 'Informe todos os digitos da matrícula.', maxlength: 'Informe todos os digitos da matrícula.'},
            senha: {required: 'Campo obrigatório.', minlength: 'Informe no mínimo 6 dígitos'},
            confirme_senha: {required: 'Campo obrigatório.', minlength: 'Informe no mínimo 6 dígitos'},
            Field: {required: 'Aceite os termos.'}
        },
        submitHandler: function (form) {
            $.ajax({
                url: 'http://sge.com.br/ajax/cadastrar_usuario.php',
                type: 'post',
                data: form_primeiro_acesso.serialize(),
                beforeSend: function () {

                },
                success: function (retorno) {
                    alert(retorno);
                    window.location.reload();
                }
            });
            return false;
        }
    });

    form_login.validate({
        rules: {
            matricula: {required: true},
            senha: {required: true}
        },
        messages: {
            matricula: {required: 'Campo obrigatório.'},
            senha: {required: 'Campo obrigatorio'}
        },
        submitHandler: function (form) {
            $.ajax({
                url: 'http://sge.com.br/ajax/logar_usuario.php',
                type: 'post',
                data: form_login.serialize(),
                beforeSend: function () {

                },
                success: function (retorno) {
                    switch (retorno) {
                        case 'Aluno':
                            window.location.href = "http://sge.com.br/aluno";
                            break;
                        case 'Professor':
                            window.location.href = "http://sge.com.br/professor";
                            break;
                        case 'Admin':
                            window.location.href = "http://sge.com.br/admin";
                            break;
                        case 'erro':
                            alert('Matricula ou Senha incorreto!');
                            window.location.reload();
                            break;
                    }
                }
            });
            return false;
        }
    });

});


