<?php

namespace app\interfaces;

interface iSql {

    public function pegar_pelo_id($campo, $valor, $tipo);

    public function cadastrar($attributes);

    public function atualizar($id, $attributes);

    public function deletar($id);

    public function listar();
}
