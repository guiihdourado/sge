<?php

namespace app\models;

abstract class AppModel extends \ActiveRecord\Model implements \app\interfaces\iSql {

    public function pegar_pelo_id($campo, $valor, $tipo) {
        return parent::find($tipo, array('conditions' => array($campo . '=?', $valor)));
    }

    public function cadastrar($attributes) {
        if (is_array($attributes)) {
            return parent::create($attributes);
        } else {
            throw new \ActiveRecord\ActiveRecordException('Valor passado deve ser um array !');
        }
    }

    public function atualizar($id, $attributes) {
        if (is_array($attributes)) {
            $atualizar = parent::find($id);
            $atualizar->update_attributes($attributes);
        } else {
            throw new \ActiveRecord\ActiveRecordException('Valor passado deve ser um array !');
        }
    }

    public function deletar($id) {
        $deletar = parent::find($id);
        $deletar->delete();
    }

    public function listar() {
        return parent::find('all');
    }

}
