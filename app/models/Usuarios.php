<?php

namespace app\models;

class Usuarios extends \app\models\AppModel {

    public static $table_name = 'tb_sge_usuario';

    public static function verificar_login($session) {
        if (!isset($_SESSION[$session])):
            header('location:../');
        endif;
    }

}
