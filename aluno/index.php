<?php
require_once '../config.php';
\app\models\Usuarios::verificar_login('usuario_logado');
?>

<!DOCTYPE html>

<html lang="pt-BR">

    <head>

        <meta charset="utf-8">

        <title>Sistema de Gerenciamento Escolar 1.0v</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <meta name="apple-mobile-web-app-capable" content="yes">

        <link href="<?php echo site_url(); ?>public/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo site_url(); ?>public/css/bootstrap-responsive.min.css" rel="stylesheet">

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

        <link href="<?php echo site_url(); ?>public/css/font-awesome.css" rel="stylesheet">

        <link href="<?php echo site_url(); ?>public/css/style.css" rel="stylesheet">

        <link href="<?php echo site_url(); ?>public/css/pages/dashboard.css" rel="stylesheet">

        <link href="<?php echo site_url(); ?>public/css/att.css" rel="stylesheet">

        <link href="<?php echo site_url(); ?>public/css/dataTables.bootstrap.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>public/css/zabuto_calendar.min.css">
        <link href="<?php echo site_url(); ?>public/css/clockpicker.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>public/css/datepicker.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>public/css/bootstrap-wysihtml5-0.0.2.css" rel="stylesheet">

        <link href="<?php echo site_url(); ?>aluno/css/aluno.css" rel="stylesheet">



    </head>

    <body>

        <div class="navbar navbar-fixed-top">

            <div class="navbar-inner">

                <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">

                        <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="?p=home">Sistema de Gerenciamento Escolar</a>

                    <div class="nav-collapse">

                        <ul class="nav pull-right">

                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-envelope"></i>  <b class="caret"></b></a>

                                <ul class="dropdown-menu">

                                    <li>

                                        <a href="#">

                                            <div>

                                                <strong>Prof. Eduardo Salinas</strong>

                                                <span class="pull-right text-muted">

                                                    <em>04/10/2014</em>

                                                </span>

                                            </div>

                                            <div>Estou aceitando o trabalho do seu grupo ate o dia 10/10/2014.
                                            </div>

                                        </a>

                                    </li>

                                    <li class="divider"></li>

                                    <li>

                                        <a href="#">

                                            <div>

                                                <strong>Prof. João Evangelista</strong>

                                                <span class="pull-right text-muted">

                                                    <em>02/10/2014</em>

                                                </span>

                                            </div>

                                            <div>Para a prova estudem os dois trabalhos valendo ponto.
                                            </div>

                                        </a>

                                    </li>

                                    <li class="divider"></li>

                                    <li>

                                        <a href="#">

                                            <div>

                                                <strong>Prof. Leandro Costa</strong>

                                                <span class="pull-right text-muted">

                                                    <em>25/09/2014</em>

                                                </span>

                                            </div>

                                            <div> A materia pra prova ja esta disbonibilizada no blog da sua turma.
                                            </div>

                                        </a>

                                    </li>

                                    <li class="divider"></li>

                                    <li>

                                        <a class="text-center" href="?p=mensagens">

                                            <strong>Visualizar Mensagens</strong>

                                            <i class="fa fa-angle-right"></i>

                                        </a>

                                    </li>

                                </ul>

                            </li>

                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i

                                        class="icon-user"></i><b class="caret"></b></a>

                                <ul class="dropdown-menu" style="width: 250px">
                                    <li class="padding-img">
                                        <img id="img-perfil" src="<?php echo site_url(); ?>public/img/usuario.jpg" width="70" align="left">                                            <p>
                                        <div class="div-p-font">
                                            <p>
                                                Jorel Vasconcelos<br/>
                                                <font>Aluno</font>
                                            </p>
                                        </div>  

                                    </li>

                                    <li class="divider" style="clear: both"></li>

                                    <li><a href="javascript:;">Perfil</a></li>

                                    <li><a href="javascript:;">Configurações</a></li>

                                    <li class="divider"></li>

                                    <li id="logout-li"><a href="inc/sair.php"><i class="icon-off"></i> Logout</a></li>

                                </ul>

                            </li>

                        </ul>

                    </div>

                    <!--/.nav-collapse --> 

                </div>

                <!-- /container --> 

            </div>

            <!-- /navbar-inner --> 

        </div>

        <!-- menu -->

        <?php include './inc/menu.php'; ?>

        <div class="main-inner">

            <div class="container">

                <div class="row">

                    <?php (isset($_GET['p'])) ? query_string($_GET['p']) : require_once 'inc/home.php'; ?>

                </div>

                <!-- /row --> 

            </div>

            <!-- /container --> 

        </div>

        <!-- /main-inner --> 

        <!-- /extra -->

        <!-- Placed at the end of the document so the pages load faster --> 

        <script src="<?php echo site_url(); ?>public/js/jquery-1.7.2.min.js"></script> 
        <script type="text/javascript" src="<?php echo site_url(); ?>public/js/jquery.validate.js"></script>
        <script src="<?php echo site_url(); ?>public/js/wysihtml5-0.3.0_rc2.js"></script>
        <script src="<?php echo site_url(); ?>public/js/excanvas.min.js"></script> 

        <script src="<?php echo site_url(); ?>public/js/chart.min.js" type="text/javascript"></script> 

        <script src="<?php echo site_url(); ?>public/js/bootstrap.js"></script>

        <script language="javascript" type="text/javascript" src="<?php echo site_url(); ?>public/js/full-calendar/fullcalendar.min.js"></script>



        <script src="<?php echo site_url(); ?>public/js/jquery.dataTables.js"></script>

        <script src="<?php echo site_url(); ?>public/js/dataTables.bootstrap.js"></script>

        <script type="text/javascript" src="<?php echo site_url(); ?>public/js/jquery.elevatezoom.js"></script>

        <script type="text/javascript" src="<?php echo site_url(); ?>public/js/bootstrap-wysiwyg.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>public/js/zabuto_calendar.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>public/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>public/js/clockpicker.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>public/js/bootstrap-wysihtml5-0.0.2.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>public/js/locales/bootstrap-datepicker.pt-BR.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>public/js/locales/bootstrap-datepicker.pt-BR.js"></script>

        <script src="js/aluno.js"></script>
        <script src="js/mensagem.js"></script>

        <script type="text/javascript">
            $('#mensagem_editor').wysihtml5({
                locale: "pt-BR",
                image: false
            });
        </script>

        <script>
            $('#dia').datepicker({
                format: "dd/mm/yyyy",
                language: "pt-BR"
            });
        </script>
        <script type="text/javascript">
            $('.clockpicker').clockpicker();
        </script>

        <script type="text/javascript">

            $('#novo-evento-calendario').tooltip();
            $('#mensagem-tooltip-1').tooltip();
            $('#mensagem-tooltip-2').tooltip();
            $('#mensagem-tooltip-3').tooltip();
            $('#mensagem-tooltip-4').tooltip();
            $('#mensagem-tooltip-5').tooltip();

        </script>



        <script type="text/javascript">

            $(document).ready(function () {

                $("#my-calendar").zabuto_calendar({
                    language: "pt",
                    ajax: {
                        url: "<?php echo site_url(); ?>aluno/json/events.json",
                        modal: true

                    }

                });

            });

        </script>



        <script type="text/javascript">

            $("#zoom_01").elevateZoom({
                zoomWindowWidth: 50,
                zoomWindowHeight: 50,
                borderSize: 2,
                borderColour: "#444"

            });

            $("#zoom_02").elevateZoom({
                zoomWindowWidth: 50,
                zoomWindowHeight: 50,
                borderSize: 2,
                borderColour: "#444"

            });

            $("#zoom_03").elevateZoom({
                zoomWindowWidth: 50,
                zoomWindowHeight: 50,
                borderSize: 2,
                borderColour: "#444"

            });

            $("#zoom_04").elevateZoom({
                zoomWindowWidth: 50,
                zoomWindowHeight: 50,
                borderSize: 2,
                borderColour: "#444"

            });

            $("#zoom_05").elevateZoom({
                zoomWindowWidth: 50,
                zoomWindowHeight: 50,
                borderSize: 2,
                borderColour: "#444"

            });

            $("#zoom_06").elevateZoom({
                zoomWindowWidth: 50,
                zoomWindowHeight: 50,
                borderSize: 2,
                borderColour: "#444"

            });

        </script>





        <script>

            $(document).ready(function () {

                $('#dataTables-admin').dataTable();

            });

        </script>



        <script type="text/javascript">

            $(document).ready(function () {

                $("#myTab a").click(function (e) {

                    e.preventDefault();

                    $(this).tab('show');

                });

            });

        </script>



        <script src="<?php echo site_url(); ?>public/js/base.js"></script>       

    </body>

</html>

