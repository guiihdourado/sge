$(document).ready(function () {
    $("#novo-evento-calendario-a").on('click', function(){
    	$("#new-event").fadeIn('slow');
    });
    
    $("#bt-admin-novo").on('click', function () {
        $("#div-admin-table").fadeOut('slow');
        $("#div-admin-novo").fadeIn(('slow'));
    });

    $("#bt-admin-fechar").on('click', function () {
        $("#div-admin-novo").fadeOut('slow');
        $("#div-admin-table").fadeIn(('slow'));
    });

    $("#select-turmas").change(function () {
        switch (parseInt($(this).val())) {
            case 0: {
                $("#1an").fadeOut().addClass("hide");
                $("#2an").fadeOut().addClass("hide");
                $("#3an").fadeOut().addClass("hide");
                $("#4an").fadeOut().addClass("hide");
                $("#5an").fadeOut().addClass("hide");
                break;
            }
            case 1: {
                $("#2an").fadeOut().addClass("hide");
                $("#3an").fadeOut().addClass("hide");
                $("#4an").fadeOut().addClass("hide");
                $("#5an").fadeOut().addClass("hide");
                $("#1an").fadeIn().removeClass("hide");
                break;
            }
            case 2: {
                $("#1an").fadeOut().addClass("hide");
                $("#3an").fadeOut().addClass("hide");
                $("#4an").fadeOut().addClass("hide");
                $("#5an").fadeOut().addClass("hide");
                $("#2an").fadeIn().removeClass("hide");
                break;
            }
            case 3: {
                $("#1an").fadeOut().addClass("hide");
                $("#2an").fadeOut().addClass("hide");
                $("#4an").fadeOut().addClass("hide");
                $("#5an").fadeOut().addClass("hide");
                $("#3an").fadeIn().removeClass("hide");
                break;
            }
            case 4: {
                $("#2an").fadeOut().addClass("hide");
                $("#3an").fadeOut().addClass("hide");
                $("#1an").fadeOut().addClass("hide");
                $("#5an").fadeOut().addClass("hide");
                $("#4an").fadeIn().removeClass("hide");
                break;
            }
            case 5: {
                $("#2an").fadeOut().addClass("hide");
                $("#3an").fadeOut().addClass("hide");
                $("#4an").fadeOut().addClass("hide");
                $("#1an").fadeOut().addClass("hide");
                $("#5an").fadeIn().removeClass("hide");
                break;
            }
        }
    });
});