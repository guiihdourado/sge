$(document).ready(function () {

    //Variaveis
    var div_admin_novo = $("#div-admin-novo");
    var div_admin_table = $("#div-admin-table");
    var form_mensagem = div_admin_novo.find('#form_mensagem');
    var mensagem_visualizar = div_admin_table.find("#mensagem_visualizar");

    $("#bt-admin-novo").on('click', function () {
        $("#div-admin-table").fadeOut('slow');
        $("#div-admin-novo").fadeIn(('slow'));
    });

    form_mensagem.validate({
        rules: {
            destinatario: {required: true},
            assunto: {required: true},
            mensagem: {required: true}

        },
        messages: {
            destinatario: {required: 'Campo obrigatório'},
            assunto: {required: 'Campo obrigatório'},
            mensagem: {required: 'Campo obrigatório'}

        },
        submitHandler: function (form) {
            $.ajax({
                url: 'http://sge.com.br/ajax/enviar_mensagem.php',
                type: 'post',
                data: form_mensagem.serialize(),
                beforeSend: function () {

                },
                success: function (retorno) {
                    window.location.reload();
                }
            });
        }

    });

    mensagem_visualizar.on('click', function () {
        var id = $(this).attr('data-id-mensagem');

        $.ajax({
            url: 'http://sge.com.br/ajax/visualizar_mensagem.php',
            type: 'post',
            data: {id: id},
            dataType: 'json',
            beforeSend: function () {

            },
            success: function (retorno) {
                var obj = $.parseJSON(retorno);
                $("#excluir_mensagem").attr('data-id', obj.id_mensagem);
                $("#remetente").html(obj.nm_pessoa);
                var dt = new Date(obj.data_envio);
                $("#data").html(dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear() + " às " + dt.getHours() + "h" + dt.getMinutes());
                $('.mensagem-corpo').html(obj.ds_mensagem);
                div_admin_table.fadeOut('slow');
                $(".mensagem-detalhada").fadeIn(('slow'));


            }
        });
    });

    $("#responder_mensagem").on('click', function () {
        $(".mensagem-detalhada").fadeOut('slow');
        $("#div-admin-novo").fadeIn(('slow'));
    });

    $("#excluir_mensagem").on('click', function () {
        var id_mensagem = $(this).attr('data-id');

        $.ajax({
            url: 'http://sge.com.br/ajax/excluir_mensagem.php',
            type: 'post',
            data: {id: id_mensagem},
            beforeSend: function () {

            },
            success: function () {
                window.location.reload();
            }
        });

    });

});