<div class="span8">

    <div class="widget">

        <div class="widget-header"> <i class="icon-bookmark"></i>

            <h3>Atalhos Importantes</h3>

        </div>

        <!-- /widget-header -->

        <div class="widget-content">

            <div class="shortcuts"> 
                <a href="?p=mensagens" class="shortcut"><i class="shortcut-icon icon-comment"></i><span class="shortcut-label">Mensagens</span> </a>
                <a href="?p=curso" class="shortcut"> <i class="shortcut-icon icon-book"></i><span class="shortcut-label">Curso</span> </a>
                <a href="?p=notas" class="shortcut"> <i class="shortcut-icon icon-tag"></i><span class="shortcut-label">Notas</span> </a>
                <!--<a href="javascript:;" class="shortcut"> <i class="shortcut-icon icon-upload"></i><span class="shortcut-label">Arquivos</span> </a>--> 
            </div>

            <!-- /shortcuts --> 

        </div>

        <!-- /widget-content --> 

    </div>

    <div class="widget">

        <div class="widget-header"> <i class="icon-bookmark"></i>

            <h3>Meu Espaço</h3>

        </div>

        <!-- /widget-header -->

        <div class="widget-content">

            <ul class="nav nav-tabs" role="tablist" id="myTab">

                <li class="active"><a href="#mensagens">Mensagens</a></li>
                <li class=""><a href="#noticias">Notícias</a></li>
                <li class=""><a href="#grade-horaria">Grade Horária</a></li>

            </ul>

            <div class="tab-content">

                <div id="mensagens" class="tab-pane fade in active">

                    <table class="table table-bordered">

                        <thead>

                            <tr>

                                <th colspan="6" style="text-align: center">Últimas Mensagens Recebidas</th>

                            </tr>

                        </thead>

                        <tbody id="mensagens">

                            <tr style="font-weight: bold">

                                <td style="width: 20px;"><img src="<?php echo site_url(); ?>public/img/envelope.png" width="20" title="Mensagem não lida" id="mensagem-tooltip-1"/></td>

                                <td style="width: 20px;"><img id="zoom_01" src="<?php echo site_url(); ?>public/img/mensagem/message_avatar2.png" data-zoom-image="<?php echo site_url(); ?>public/img/mensagem/message_avatar2.png" width="20"></td>

                                <td style="width: 180px;"><a href="?p=mensagens&id=1">Prof. Eduardo Salinas</a></td>

                                <td style="width: 30px;">30/09</td>

                                <td><a href="?p=mensagens&id=1">Envio de Trabalho</a></td>

                            </tr>

                            <tr style="font-weight: bold">

                                <td style="width: 20px;"><img src="<?php echo site_url(); ?>public/img/envelope.png" width="20" title="Mensagem não lida" id="mensagem-tooltip-2"/></td>

                                <td style="width: 20px;"><img id="zoom_02" src="<?php echo site_url(); ?>public/img/mensagem/message_avatar1.png" data-zoom-image="<?php echo site_url(); ?>public/img/mensagem/message_avatar1.png" width="20"></td>

                                <td style="width: 180px;"><a href="?p=mensagens&id=2">Prof. João Evangelista</a></td>

                                <td style="width: 30px;">30/09</td>

                                <td><a href="?p=mensagens&id=2">Prova</a></td>

                            </tr>

                            <tr>

                                <td style="width: 20px;"><img src="<?php echo site_url(); ?>public/img/envelope-open.png" width="20" title="Mensagem lida" id="mensagem-tooltip-5"/></td>

                                <td style="width: 20px;"><img id="zoom_05" src="<?php echo site_url(); ?>public/img/mensagem/message_avatar5.jpg" data-zoom-image="<?php echo site_url(); ?>public/img/mensagem/message_avatar5.jpg" width="20"></td>

                                <td style="width: 180px;"><a href="?p=mensagens&id=5">Prof. Leandro Costa</a></td>

                                <td style="width: 30px;">30/09</td>

                                <td><a href="?p=mensagens&id=5">Materia pra prova</a></td>

                            </tr>



                        </tbody>

                    </table>

                </div>

                <div id="noticias" class="tab-pane fade">

                    <table class="table table-bordered">

                        <thead>

                            <tr>

                                <th colspan="6" style="text-align: center">Notícias</th>

                            </tr>

                        </thead>

                        <tbody id="noticias">  

                            <tr>

                                <td style="width: 200px;"><a href="?p=noticias&id=1">Administrador</a></td>

                                <td style="width: 100px;">06/10</td>

                                <td><a href="?p=noticias&id=1">Notas do seminário já estão disponíveis no blog</a></td>

                            </tr>

                            <tr>

                                <td style="width: 200px;"><a href="?p=noticias&id=2">Administrador</a></td>

                                <td style="width: 100px;">10/10</td>

                                <td><a href="?p=noticias&id=2">Como estão os estágios na área de TI  </a></td>

                            </tr>

                            <tr>

                                <td style="width: 200px;"><a href="?p=noticias&id=3">Administrador</a></td>

                                <td style="width: 100px;">20/10</td>

                                <td><a href="?p=noticias&id=3">Inscrições para Palestra - Open Source </a></td>

                            </tr>

                            <tr>

                                <td style="width: 200px;"><a href="?p=noticias&id=4">Administrador</a></td>

                                <td style="width: 100px;">22/10</td>

                                <td><a href="?p=noticias&id=4">Notas da prova A1 já estão disponíveis no blog</a></td>
                            </tr>



                        </tbody>

                    </table>


                </div>

                <div id="grade-horaria" class="tab-pane fade">
                    <table class="table table-bordered">

                        <thead>

                            <tr>

                                <th><center>Horário</center></th>
                        <th><center>Segunda</center></th>
                        <th><center>Terça</center></th>
                        <th><center>Quarta</center></th>
                        <th><center>Quinta</center></th>
                        <th><center>Sexta</center></th>
                        <th><center>Sábado</center></th>

                        </tr>

                        </thead>

                        <tbody id="mensagens">
                            <tr>
                                <td><center>19h30 às 21h00</center></td>
                        <td><center>Projeto Integrado II - Sala 01</center></td>
                        <td><center>Engenharia de Software I - Sala 02</center></td>
                        <td><center> --- </center></td>
                        <td><center>Projeto Integrado III - Sala 01 </center></td>
                        <td><center>Engenharia de Software II - Sala 02</center></td>
                        <td><center> --- </center></td>
                        </tr>
                        <tr>
                            <td><center>21h15 às 22h55</center></td>
                        <td><center>Projeto Integrado II - Sala 01</center></td>
                        <td><center>Engenharia de Software I - Sala 02</center></td>
                        <td><center> --- </center></td>
                        <td><center>Projeto Integrado III - Sala 01 </center></td>
                        <td><center>Engenharia de Software II - Sala 02</center></td>
                        <td><center> --- </center></td>
                        </tr>

                        </tbody>

                    </table>

                </div>

            </div>


        </div>

        <!-- /widget-content --> 

    </div>

</div>

<!-- /span6 -->

<div class="span4">

    <!-- /widget -->

    <div class="widget">

        <div class="widget-header"> <i class="icon-user"></i>
            <h3> Aluno</h3>
        </div>

        <!-- /widget-header -->

        <div class="widget-content">
            <div class="user-heading round">
                <a href="#">
                    <img src="<?php echo site_url(); ?>public/img/usuario.jpg" alt="">
                </a>
                <h1>Jorel Vasconcelos</h1>
                <p>201359254</p>
                <p>vasconcelosjorel@gmail.com</p>
                <p>(00) 0000-0000</p>
            </div>
        </div>

        <!-- /widget-content --> 
    </div>

    <!-- /widget -->

    <div class="widget">

        <div class="widget-header"> <i class="icon-calendar"></i>

            <h3> Calendário</h3>

            <a id="novo-evento-calendario-a" href="#myModal" data-toggle="modal" data-target="#myModal"><i class="icon-plus-sign" id="novo-evento-calendario" title="Adicionar Evento ao Calendário"></i></a>

        </div>

        <!-- /widget-header -->

        <div class="widget-content">

            <div id="my-calendar"></div>

        </div>

        <!-- /widget-content --> 

        <div id="myModal" class="modal fade hide">
            <div class="modal-header">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h3>Criar Evento</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">

                    <div class="control-group">                                         

                        <label class="control-label" for="titulo">Título</label>

                        <div class="controls">

                            <input type="text" class="span3" id="titulo"  placeholder="Título do Evento">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->



                    <div class="control-group">                                         

                        <label class="control-label" for="dia">Dia do Evento</label>

                        <div class="controls" id="datepicker">

                            <input type="text" class="span3" id="dia"  placeholder="Dia do Evento">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->

                    <div class="control-group">                                         

                        <label class="control-label" for="hora">Hora</label>

                        <div class="controls clockpicker" data-autoclose="true">

                            <input type="text" class="span3" id="hora"  placeholder="Horas do Evento">

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->



                    <div class="control-group">                                         

                        <label class="control-label" for="hora">Descrição</label>

                        <div class="controls">

                            <textarea id="descricao" class="span3" name="descricao"  placeholder="Descrição do Evento"></textarea>

                        </div> <!-- /controls -->               

                    </div> <!-- /control-group -->




            </div>
            <div class="modal-footer">
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-primary">Cadastrar</button> 
                        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>

                    </div>

                </div>

                </form>

            </div>
        </div>

    </div>

</div>

<!-- /span6 -->
