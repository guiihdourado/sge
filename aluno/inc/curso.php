<div class="span12">
  <div class="div-admin-breadcrum">
    <ul class="breadcrumb">
      <li>
        <a href="?p=home">Início</a>
        <span class="divider">/</span>
      </li>
      <li class="active">Curso</li>
    </ul>
  </div>

  <div class="widget widget-table action-table">
    <div class="widget-header">
      <i class="icon-th-list"></i>

      <h3>Curso</h3>

    </div>

    <div class="widget-content">

      <div class="span3 menu">

        <div class="panel panel-default">

          <div class="panel-body bg-grey-lighter">
            <aside class="profile-nav">
              <section class="panel">
                <ul class="nav nav-pills nav-stacked">
                  <li class="active">
                    <a href="#disciplina" data-toggle="tab"> 1.Disciplinas</a>
                  </li>
                  <li>
                    <a href="#turmas" data-toggle="tab">2.Turmas</a>
                  </li>
                  <li>
                    <a href="#historico" data-toggle="tab">3.Histórico</a>
                  </li>
                  <li>
                    <a href="#notas" data-toggle="tab">4.Frequências e Notas</a>
                  </li>
                </ul>
              </section>
            </aside>
          </div>
        </div>

      </div>

      <div class="span8">
        <div class="panel panel-default">
          <div class="panel-body bg-grey-lighter">

            <div class="tab-content content-tabs">

              <div class="tab-pane fade active in" id="disciplina">
                <fieldset>
                  <legend class="font-15px color-blue">1. Disciplinas</legend>
                  <!-- /widget-header -->

                  <div class="widget-content" style="border: none;">


                    <ul class="nav nav-tabs" role="tablist" id="myTab">

                      <li class="active">
                        <a href="#primeiro">1º Período</a>
                      </li>
                      <li class="">
                        <a href="#segundo">2º Período</a>
                      </li>
                      <li class="">
                        <a href="#terceiro">3º Período</a>
                      </li>
                      <li class="">
                        <a href="#quarto">4º Período</a>
                      </li>
                      <li class="">
                        <a href="#quinto">5º Período</a>
                      </li>

                    </ul>
                    <!-- /widget-header -->

                    <div class="tab-content">
                      <div id="primeiro" class="tab-pane fade in active">
                        <div class="table-responsive padding-table" >

                          <table class="table table-striped table-bordered table-hover">

                            <thead>

                              <tr>

                                <th>Disciplina</th>

                                <th style="width: 50px">Carga Horária</th>

                                <th style="width: 50px">Frequência</th>

                                <th style="width: 100px;">Status</th>


                              </tr>

                            </thead>

                            <tbody>
                              <tr>
                                <td>Fundamentos da Computação</td>
                                <td>80h</td>
                                <td>100%</td>
                                <td>Cursada</td>
                              </tr>

                              <tr>
                                <td>Lógica Matemática</td>
                                <td>80</td>
                                <td>100%</td>
                                <td>Cursada</td>
                              </tr>

                              <tr>
                                <td>Lógica de Programação</td>
                                <td>80h</td>
                                <td>100%</td>
                                <td>Cursada</td>
                              </tr>

                              <tr>
                                <td>Metodologia de Ensino e Pesquisa</td>
                                <td>40h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                              <tr>
                                <td>Design para Web</td>
                                <td>80h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                              <tr>
                                <td>Arquitetura de Computadores</td>
                                <td>80h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                            </tbody>

                          </table>

                        </div>
                      </div>

                      <div id="segundo" class="tab-pane fade">
                        <div class="table-responsive padding-table" >

                          <table class="table table-striped table-bordered table-hover" id="dataTables-admin2">

                            <thead>

                              <tr>

                                <th>Disciplina</th>

                                <th style="width: 50px">Carga Horária</th>

                                <th style="width: 50px">Frequência</th>

                                <th style="width: 100px;">Status</th>


                              </tr>

                            </thead>

                            <tbody>

                              <tr>
                                <td>Banco de Dados I</td>
                                <td>80h</td>
                                <td>95%</td>
                                <td>Cursada</td>
                              </tr>

                              <tr>
                                <td>Aplicação WEB</td>
                                <td>80h</td>
                                <td>95%</td>
                                <td>Cursada</td>
                              </tr>

                              <tr>
                                <td>Legislação Aplicada a Computação</td>
                                <td>40h</td>
                                <td>100%</td>
                                <td>Cursada</td>
                              </tr>

                              <tr>
                                <td>Linguagem e Técnica de Programação</td>
                                <td>80h</td>
                                <td>100%</td>
                                <td>Cursada</td>
                              </tr>

                              <tr>
                                <td>Probabilidade e Estatísticas</td>
                                <td>80h</td>
                                <td>80%</td>
                                <td>Cursada</td>
                              </tr>

                              <tr>
                                <td>Redes de Computadores I</td>
                                <td>80h</td>
                                <td>95%</td>
                                <td>Cursada</td>
                              </tr>

                            </tbody>

                          </table>

                        </div>
                      </div>

                      <div id="terceiro" class="tab-pane fade">
                        <div class="table-responsive padding-table" >

                          <table class="table table-striped table-bordered table-hover" id="dataTables-admin3">

                            <thead>

                              <tr>

                                <th>Disciplina</th>

                                <th style="width: 50px">Carga Horária</th>

                                <th style="width: 50px">Frequência</th>

                                <th style="width: 100px;">Status</th>


                              </tr>

                            </thead>

                            <tbody>
                              <tr>
                                <td>Banco de Dados II</td>
                                <td>80h</td>
                                <td>100%</td>
                                <td>Cursada</td>
                              </tr>

                              <tr>
                                <td>Engenharia de Software</td>
                                <td>80h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                              <tr>
                                <td>Linguagem e Técnica de Programação II</td>
                                <td>80h</td>
                                <td>90%</td>
                                <td>Cursada</td>
                              </tr>

                              <tr>
                                <td>Projeto Integrado I</td>
                                <td>80h</td>
                                <td>80%</td>
                                <td>Cursada</td>
                              </tr>

                              <tr>
                                <td>Sistemas Operacionais</td>
                                <td>80h</td>
                                <td>100%</td>
                                <td>Cursada</td>
                              </tr>
                            </tbody>

                          </table>

                        </div>
                      </div>

                      <div id="quarto" class="tab-pane fade">
                        <div class="table-responsive padding-table" >

                          <table class="table table-striped table-bordered table-hover" id="dataTables-admin4">

                            <thead>

                              <tr>

                                <th>Disciplina</th>

                                <th style="width: 50px">Carga Horária</th>

                                <th style="width: 50px">Frequência</th>

                                <th style="width: 100px;">Status</th>


                              </tr>

                            </thead>

                            <tbody>
                              <tr>
                                <td>Empreendedorismo</td>
                                <td>80h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                              <tr>
                                <td>Engenharia de Software II</td>
                                <td>80h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                              <tr>
                                <td>Linguagem e Técnica de Programação III</td>
                                <td>40h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                              <tr>
                                <td>Estrutura de Dados</td>
                                <td>80h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                              <tr>
                                <td>Segurança de Sistemas</td>
                                <td>80h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                            </tbody>

                          </table>

                        </div>
                      </div>

                      <div id="quinto" class="tab-pane fade">
                        <div class="table-responsive padding-table" >

                          <table class="table table-striped table-bordered table-hover" id="dataTables-admin4">

                            <thead>

                              <tr>

                                <th>Disciplina</th>

                                <th style="width: 50px">Carga Horária</th>

                                <th style="width: 50px">Frequência</th>

                                <th style="width: 100px;">Status</th>


                              </tr>

                            </thead>

                            <tbody>
                              <tr>
                                <td>Empreendedorismo</td>
                                <td>80h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                              <tr>
                                <td>Engenharia de Software II</td>
                                <td>80h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                              <tr>
                                <td>Linguagem e Técnica de Programação III</td>
                                <td>40h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                              <tr>
                                <td>Estrutura de Dados</td>
                                <td>80h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                              <tr>
                                <td>Segurança de Sistemas</td>
                                <td>80h</td>
                                <td>- - -</td>
                                <td>A Cursar</td>
                              </tr>

                            </tbody>

                          </table>

                        </div>
                      </div>
                    </div>
                  </div>

                </fieldset>
              </div>

              <div class="tab-pane fade " id="turmas">
                <fieldset>
                  <legend class="font-15px color-blue">2. Turmas</legend>
                  <div class="widget-content" style="border: none;">
                    <select class="span7" id="select-turmas">
                      <option value="0">-- Selecione --</option>
                      <option value="1">TADS 1AN</option>
                      <option value="2">TADS 2AN</option>
                      <option value="3">TADS 3AN</option>
                    </select>

                    <div id="1an" class="table-responsive padding-table hide" >

                      <table class="table table-striped table-bordered table-hover">

                        <thead>

                          <tr>

                            <th>Disciplina</th>

                            <th style="width: 50px">Carga Horária</th>

                            <th style="width: 50px">Frequência</th>

                            <th style="width: 100px;">Status</th>


                          </tr>

                        </thead>

                        <tbody>
                          <tr>
                            <td>Fundamentos da Computação</td>
                            <td>80h</td>
                            <td>100%</td>
                            <td>Cursada</td>
                          </tr>

                          <tr>
                            <td>Lógica Matemática</td>
                            <td>80</td>
                            <td>100%</td>
                            <td>Cursada</td>
                          </tr>

                          <tr>
                            <td>Lógica de Programação</td>
                            <td>80h</td>
                            <td>100%</td>
                            <td>Cursada</td>
                          </tr>

                          <tr>
                            <td>Metodologia de Ensino e Pesquisa</td>
                            <td>40h</td>
                            <td>- - -</td>
                            <td>A Cursar</td>
                          </tr>

                          <tr>
                            <td>Design para Web</td>
                            <td>80h</td>
                            <td>- - -</td>
                            <td>A Cursar</td>
                          </tr>

                          <tr>
                            <td>Arquitetura de Computadores</td>
                            <td>80h</td>
                            <td>- - -</td>
                            <td>A Cursar</td>
                          </tr>

                        </tbody>

                      </table>

                    </div>

                    <div id="2an" class="table-responsive padding-table hide" >

                      <table class="table table-striped table-bordered table-hover" id="dataTables-admin2">

                        <thead>

                          <tr>

                            <th>Disciplina</th>

                            <th style="width: 50px">Carga Horária</th>

                            <th style="width: 50px">Frequência</th>

                            <th style="width: 100px;">Status</th>


                          </tr>

                        </thead>

                        <tbody>

                          <tr>
                            <td>Banco de Dados I</td>
                            <td>80h</td>
                            <td>95%</td>
                            <td>Cursada</td>
                          </tr>

                          <tr>
                            <td>Aplicação WEB</td>
                            <td>80h</td>
                            <td>95%</td>
                            <td>Cursada</td>
                          </tr>

                          <tr>
                            <td>Legislação Aplicada a Computação</td>
                            <td>40h</td>
                            <td>100%</td>
                            <td>Cursada</td>
                          </tr>

                          <tr>
                            <td>Linguagem e Técnica de Programação</td>
                            <td>80h</td>
                            <td>100%</td>
                            <td>Cursada</td>
                          </tr>

                          <tr>
                            <td>Probabilidade e Estatísticas</td>
                            <td>80h</td>
                            <td>80%</td>
                            <td>Cursada</td>
                          </tr>

                          <tr>
                            <td>Redes de Computadores I</td>
                            <td>80h</td>
                            <td>95%</td>
                            <td>Cursada</td>
                          </tr>

                        </tbody>

                      </table>

                    </div>

                    <div id="3an" class="table-responsive padding-table hide" >

                      <table class="table table-striped table-bordered table-hover" id="dataTables-admin4">

                        <thead>

                          <tr>

                            <th>Disciplina</th>

                            <th style="width: 50px">Carga Horária</th>

                            <th style="width: 50px">Frequência</th>

                            <th style="width: 100px;">Status</th>


                          </tr>

                        </thead>

                        <tbody>
                          <tr>
                            <td>Empreendedorismo</td>
                            <td>80h</td>
                            <td>- - -</td>
                            <td>Cursando</td>
                          </tr>

                          <tr>
                            <td>Engenharia de Software II</td>
                            <td>80h</td>
                            <td>- - -</td>
                            <td>Cursando</td>
                          </tr>

                          <tr>
                            <td>Linguagem e Técnica de Programação III</td>
                            <td>40h</td>
                            <td>- - -</td>
                            <td>Cursando</td>
                          </tr>

                          <tr>
                            <td>Estrutura de Dados</td>
                            <td>80h</td>
                            <td>- - -</td>
                            <td>Cursando</td>
                          </tr>

                          <tr>
                            <td>Segurança de Sistemas</td>
                            <td>80h</td>
                            <td>- - -</td>
                            <td>Cursando</td>
                          </tr>

                        </tbody>

                      </table>

                    </div>
                  </div>
                </fieldset>
              </div>

              <div class="tab-pane fade " id="historico">
                <fieldset>
                  <legend class="font-15px color-blue">3. Histórico</legend>
                </fieldset>
              </div>

              <div class="tab-pane fade " id="notas">
                <fieldset>
                  <legend class="font-15px color-blue">4. Frequências e Notas</legend>
                </fieldset>
              </div>

            </div>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>
