<div class="span12">
    <div class="div-admin-breadcrum">
        <?php
        if ($_SERVER['QUERY_STRING'] == 'p=notas') {
            ?>
            <ul class="breadcrumb">
                <li><a href="?p=home">Início</a> <span class="divider">/</span></li>
                <li class="active">Notas</li>
            </ul>
        </div>

        <?php
    }
    ?>

    <div class="widget widget-table action-table">

        <div class="widget-header"> <i class="icon-th-list"></i>

            <h3>Notas</h3>

        </div>

        <!-- /widget-header -->

        <div class="widget-content">

            <div class="padding-div">

                <ul class="nav nav-tabs" role="tablist" id="myTab">

                    <li class="active"><a href="#2013-1">2013/01</a></li>
                    <li class=""><a href="#2013-2">2013/02</a></li>
                    <li class=""><a href="#2014-1">2014/01</a></li>
                    <li class=""><a href="#2014-2">2014/02</a></li>

                </ul>
                <!-- /widget-header -->

                <div class="tab-content">
                    <div id="2013-1" class="tab-pane fade in active">
                        <div class="table-responsive padding-table" >

                            <table class="table table-striped table-bordered table-hover">

                                <thead>

                                    <tr>

                                        <th>Disciplina</th>

                                        <th style="width: 50px">A1</th>

                                        <th style="width:50px">A2</th>

                                        <th style="width: 50px;">Média</th>

                                        <th style="width: 100px;">Status</th>


                                    </tr>

                                </thead>

                                <tbody>                 
                                    <tr>
                                        <td>Fundamentos da Computação</td>
                                        <td>6,5</td>
                                        <td>7,0</td>
                                        <td>6,75</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Lógica Matemática</td>
                                        <td>5,0</td>
                                        <td>8,5</td>
                                        <td>6,75</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Lógica de Programação</td>
                                        <td>4,0</td>
                                        <td>6,0</td>
                                        <td>5,0</td>
                                        <td>Reprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Metodologia de Ensino e Pesquisa</td>
                                        <td>8,0</td>
                                        <td>7,0</td>
                                        <td>7,5</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Design para Web</td>
                                        <td>8,5</td>
                                        <td>8,5</td>
                                        <td>8,5</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Arquitetura de Computadores</td>
                                        <td>6,5</td>
                                        <td>9,0</td>
                                        <td>7,75</td>
                                        <td>Aprovado</td>
                                    </tr>

                                </tbody>

                            </table>

                        </div>
                    </div>

                    <div id="2013-2" class="tab-pane fade">
                        <div class="table-responsive padding-table" >

                            <table class="table table-striped table-bordered table-hover" id="dataTables-admin2">

                                <thead>

                                    <tr>

                                        <th>Disciplina</th>

                                        <th style="width: 50px">A1</th>

                                        <th style="width:50px">A2</th>

                                        <th style="width: 50px;">Média</th>

                                        <th style="width: 100px;">Status</th>


                                    </tr>

                                </thead>

                                <tbody>                 

                                    <tr>
                                        <td>Banco de Dados I</td>
                                        <td>6,5</td>
                                        <td>7,0</td>
                                        <td>6,75</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Aplicação WEB</td>
                                        <td>5,0</td>
                                        <td>8,5</td>
                                        <td>6,75</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Legislação Aplicada a Computação</td>
                                        <td>4,0</td>
                                        <td>6,0</td>
                                        <td>5,0</td>
                                        <td>Reprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Linguagem e Técnica de Programação</td>
                                        <td>8,0</td>
                                        <td>7,0</td>
                                        <td>7,5</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Probabilidade e Estatísticas</td>
                                        <td>8,5</td>
                                        <td>8,5</td>
                                        <td>8,5</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Redes de Computadores I</td>
                                        <td>6,5</td>
                                        <td>9,0</td>
                                        <td>7,75</td>
                                        <td>Aprovado</td>
                                    </tr>

                                </tbody>

                            </table>

                        </div>
                    </div>

                    <div id="2014-1" class="tab-pane fade">
                        <div class="table-responsive padding-table" >

                            <table class="table table-striped table-bordered table-hover" id="dataTables-admin3">

                                <thead>

                                    <tr>

                                        <th>Disciplina</th>

                                        <th style="width: 50px">A1</th>

                                        <th style="width:50px">A2</th>

                                        <th style="width: 50px;">Média</th>

                                        <th style="width: 100px;">Status</th>


                                    </tr>

                                </thead>

                                <tbody>                 
                                    <tr>
                                        <td>Banco de Dados II</td>
                                        <td>6,5</td>
                                        <td>7,0</td>
                                        <td>6,75</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Engenharia de Software</td>
                                        <td>5,0</td>
                                        <td>8,5</td>
                                        <td>6,75</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Linguagem e Técnica de Programação II</td>
                                        <td>8,0</td>
                                        <td>7,0</td>
                                        <td>7,5</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Projeto Integrado I</td>
                                        <td>8,5</td>
                                        <td>8,5</td>
                                        <td>8,5</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Sistemas Operacionais</td>
                                        <td>6,5</td>
                                        <td>9,0</td>
                                        <td>7,75</td>
                                        <td>Aprovado</td>
                                    </tr>
                                </tbody>

                            </table>

                        </div>
                    </div>

                    <div id="2014-2" class="tab-pane fade">
                        <div class="table-responsive padding-table" >

                            <table class="table table-striped table-bordered table-hover" id="dataTables-admin4">

                                <thead>

                                    <tr>

                                        <th>Disciplina</th>

                                        <th style="width: 50px">A1</th>

                                        <th style="width:50px">A2</th>

                                        <th style="width: 50px;">Média</th>

                                        <th style="width: 100px;">Status</th>


                                    </tr>

                                </thead>

                                <tbody>                 
                                    <tr>
                                        <td>Empreendedorismo</td>
                                        <td>6,5</td>
                                        <td>7,0</td>
                                        <td>6,75</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Engenharia de Software II</td>
                                        <td>5,0</td>
                                        <td>8,5</td>
                                        <td>6,75</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Linguagem e Técnica de Programação III</td>
                                        <td>8,0</td>
                                        <td>7,0</td>
                                        <td>7,5</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Estrutura de Dados</td>
                                        <td>8,5</td>
                                        <td>8,5</td>
                                        <td>8,5</td>
                                        <td>Aprovado</td>
                                    </tr>

                                    <tr>
                                        <td>Segurança de Sistemas</td>
                                        <td>6,5</td>
                                        <td>9,0</td>
                                        <td>7,75</td>
                                        <td>Aprovado</td>
                                    </tr>

                                </tbody>

                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

</div>