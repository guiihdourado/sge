<div class="span12">

    <div class="error-container">
        <h1>404</h1>

        <h2>Página não existe !</h2>

        <div class="error-details">
            Desculpe, ocorreu um erro! Tente voltar para a pagina inicial.

        </div> <!-- /error-details -->

        <div class="error-actions">
            <a href="?p=home" class="btn btn-large btn-primary">
                <i class="icon-chevron-left"></i>
                &nbsp;
                Voltar para o Inicio						
            </a>



        </div> <!-- /error-actions -->

    </div> <!-- /error-container -->			

</div> <!-- /span12 -->